import { clerkMiddleware, createRouteMatcher } from '@clerk/nextjs/server';
import createIntlMiddleware from 'next-intl/middleware';

import { appConfig } from '@/app.config';

// Middleware untuk menangani routing internasionalisasi
const intlMiddleware = createIntlMiddleware({
  locales: appConfig.i18n.locales,
  defaultLocale: appConfig.i18n.defaultLocale,
  localeDetection: appConfig.i18n.localeDetection,
  localePrefix: appConfig.i18n.localePrefix,
});

const isProtectedRoutes = createRouteMatcher(['/']);

export default clerkMiddleware((auth, req) => {
  if (isProtectedRoutes(req)) auth().protect();

  return intlMiddleware(req);
});

export const config = {
  matcher: ['/((?!.+\\.[\\w]+$|_next).*)', '/', '/(api|trpc)(.*)'],
};
