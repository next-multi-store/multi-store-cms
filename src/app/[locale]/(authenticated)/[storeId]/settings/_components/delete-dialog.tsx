import {
  AlertDialog,
  AlertDialogAction,
  AlertDialogCancel,
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogTitle,
} from "@/components/ui/alert-dialog";
import { StoreDB } from "@/schemas/db";

interface DeleteDialogProps {
  isOpen: boolean;
  isLoading: boolean;
  data: StoreDB | null;
  onHandleSubmitDelete: (store: StoreDB) => void;
  onCloseDialog: () => void;
}

const DeleteDialog = ({
  isLoading,
  isOpen,
  data,
  onHandleSubmitDelete,
  onCloseDialog,
}: DeleteDialogProps) => {
  return (
    <AlertDialog
      onOpenChange={(isOpen) => {
        if (!isOpen) onCloseDialog();
      }}
      open={isOpen}>
      <AlertDialogContent>
        <AlertDialogHeader>
          <AlertDialogTitle>Warning</AlertDialogTitle>
          <AlertDialogDescription>
            Are you sure want to delete data {data && data.name}
          </AlertDialogDescription>
        </AlertDialogHeader>
        <AlertDialogFooter>
          <AlertDialogCancel
            onClick={() => onCloseDialog()}
            disabled={isLoading}>
            Cancel
          </AlertDialogCancel>
          <AlertDialogAction
            onClick={() => onHandleSubmitDelete(data!)}
            disabled={isLoading}>
            Continue
          </AlertDialogAction>
        </AlertDialogFooter>
      </AlertDialogContent>
    </AlertDialog>
  );
};

export { DeleteDialog };
