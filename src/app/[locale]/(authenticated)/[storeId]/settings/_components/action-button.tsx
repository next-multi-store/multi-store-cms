'use client';
import { Button } from '@/components/ui/button';
import { Trash } from 'lucide-react';
import React, { useState } from 'react';
import { DeleteDialog } from './delete-dialog';
import { StoreDB } from '@/schemas/db';
import { toast } from '@/hooks/use-toast';
import axios from 'axios';
import { useRouter } from 'next/navigation';
import { ButtonCopy } from '@/components/button-copy';
import { useOriginUrl } from '@/hooks/use-origin-url';

interface ButtonDeleteProps {
  data: StoreDB | null;
}

const ActionButton = ({ data }: ButtonDeleteProps) => {
  const [isOpenDialog, setIsOpenDialog] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const origin: string | null = useOriginUrl();
  const handleShowDialog = (value: boolean) => {
    setIsOpenDialog(value);
  };
  const router = useRouter();

  const handleDelete = async (data: StoreDB) => {
    setIsLoading(true);
    try {
      const { status } = await fetch(`/api/billboards`);
      //const { status } = await axios.delete(`/api/stores/${data.id}`);
      if (status !== 204)
        throw new Error('Failed to update data. Please try again.');

      toast({
        title: 'Success',
        description: `${data.name} has been deleted`,
        variant: 'success',
      });
      router.refresh();
    } catch (error) {
      toast({
        title: 'Error',
        description: `${data.name} error while saving`,
        variant: 'error',
      });
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <>
      <div className="flex items-center justify-center gap-3">
        <Button
          variant={'destructive'}
          size={'icon'}
          onClick={() => handleShowDialog(true)}
          className="w-full md:w-9"
        >
          <Trash className="size-4 min-h-4 min-w-4" />
        </Button>
        <ButtonCopy description={`${origin}/api/${data?.id ?? ''}`} />
      </div>
      <DeleteDialog
        isLoading={isLoading}
        isOpen={isOpenDialog}
        data={data ?? null}
        onCloseDialog={() => handleShowDialog(false)}
        onHandleSubmitDelete={handleDelete}
      />
    </>
  );
};

export { ActionButton };
