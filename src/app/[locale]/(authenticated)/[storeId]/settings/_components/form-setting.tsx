'use client';

import { Button } from '@/components/ui/button';
import { Form, FormField } from '@/components/ui/form';
import { Input } from '@/components/ui/input';
import { toast } from '@/hooks/use-toast';
import { Store, storeSchema } from '@/schemas/z-schema';
import { StoreDB } from '@/schemas/db';
import { zodResolver } from '@hookform/resolvers/zod';
import axios from 'axios';
import { useParams, useRouter } from 'next/navigation';
import { useEffect, useMemo, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useTranslations } from 'next-intl';
import { Textarea } from '@/components/ui/textarea';
import { Gmaps } from '@/components/gmaps';
import debounce from 'lodash/debounce';

interface FormProps {
  initData: StoreDB | null;
}

const FormSetting = ({ initData }: FormProps) => {
  const { storeId } = useParams();
  const [isFetchingCoordinates, setIsFetchingCoordinates] =
    useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const router = useRouter();
  const t = useTranslations();
  const labelWidth = 350;
  const defaultValues: Store = useMemo(
    () => ({
      address: '',
      city: '',
      imageUrl: '',
      latitude: undefined,
      longitude: undefined,
      name: '',
      phone: '',
      zipCode: '',
    }),
    [],
  );

  const form = useForm<Store>({
    resolver: zodResolver(storeSchema),
    defaultValues,
  });

  useEffect(() => {
    if (initData) {
      form.reset({
        id: initData.id,
        address: initData?.address ?? '',
        city: initData?.city ?? '',
        latitude: initData?.latitude ?? undefined,
        longitude: initData?.longitude ?? undefined,
        name: initData?.name ?? '',
        phone: initData?.phone ?? '',
        zipCode: initData?.zipCode ?? '',
      });

      if (initData?.latitude && initData?.longitude) {
        setMarker({ lat: initData?.latitude, lng: initData?.longitude });
      }
    } else {
      form.reset(defaultValues);
    }
  }, [initData, form, defaultValues]);

  const handleSubmit = async (data: Store) => {
    setIsLoading(true);
    try {
      const { status } = await axios.patch(`/api/stores/${storeId}`, data);
      if (status !== 200)
        throw new Error('Failed to update data. Please try again.');

      toast({
        title: 'Success',
        description: `${data.name} has been updated`,
        variant: 'success',
      });

      router.refresh();
    } catch (error) {
      toast({
        title: 'Error',
        description: `${data.name} error while saving`,
        variant: 'error',
      });
    } finally {
      setIsLoading(false);
    }
  };

  const [marker, setMarker] = useState<{ lat: number; lng: number } | null>(
    null,
  );

  const handlePinMap = (event: google.maps.MapMouseEvent) => {
    if (event) {
      const newMarker = {
        lat: event.latLng!.lat(),
        lng: event.latLng!.lng(),
      };
      form.setValue('latitude', event.latLng!.lat());
      form.setValue('longitude', event.latLng!.lng());
      setMarker(newMarker);
    }
  };

  const handleGetAddressFromCoordinates = async ({
    lat,
    lng,
  }: {
    lat: number;
    lng: number;
  }) => {
    if (isFetchingCoordinates) return;

    setIsFetchingCoordinates(true);
    const { data } = await axios.get(`/api/${storeId}/maps`, {
      params: {
        lat,
        lng,
      },
    });

    const { data: dataMap } = data;
    form.setValue('address', dataMap.address);
    form.setValue('city', dataMap.city);
    form.setValue('zipCode', dataMap.zipCode);
    setIsFetchingCoordinates(false);
  };

  const debouncedGetAddress = debounce(handleGetAddressFromCoordinates, 500);

  useEffect(() => {
    if (
      marker &&
      marker.lat &&
      marker.lng &&
      (initData?.latitude !== marker.lat || initData?.longitude !== marker.lng)
    ) {
      debouncedGetAddress({ lat: marker.lat, lng: marker.lng });
    }
  }, [marker]);

  return (
    <Form {...form}>
      <form
        onSubmit={form.handleSubmit(handleSubmit)}
        className="w-full mt-6 flex flex-col gap-3"
      >
        <FormField
          control={form.control}
          name="name"
          render={({ field }) => {
            return (
              <Input
                disabled={isLoading}
                hasForm
                isResponsive
                autoComplete="off"
                autoFocus={false}
                label={t('stores.form.label.name')}
                labelWidth={labelWidth}
                placeholder={t('stores.form.placeholder.name')}
                required
                {...field}
              />
            );
          }}
        />

        <Gmaps
          label="Maps"
          labelWidth={labelWidth}
          isResponsive
          onHandlePinMap={handlePinMap}
          marker={marker}
        />

        <FormField
          control={form.control}
          name="address"
          render={({ field }) => {
            return (
              <Textarea
                hasForm
                isResponsive
                readOnly={isFetchingCoordinates}
                autoComplete="off"
                autoFocus={false}
                label={t('stores.form.label.address')}
                labelWidth={labelWidth}
                placeholder={t('stores.form.placeholder.address')}
                required
                disabled={isLoading || isFetchingCoordinates}
                {...field}
              />
            );
          }}
        />

        <FormField
          control={form.control}
          name="city"
          render={({ field }) => {
            return (
              <Input
                disabled={isLoading || isFetchingCoordinates}
                hasForm
                isResponsive
                readOnly={isFetchingCoordinates}
                autoComplete="off"
                autoFocus={false}
                label={t('stores.form.label.city')}
                labelWidth={labelWidth}
                placeholder={t('stores.form.placeholder.city')}
                required
                {...field}
              />
            );
          }}
        />

        <FormField
          control={form.control}
          name="zipCode"
          render={({ field }) => {
            return (
              <Input
                disabled={isLoading || isFetchingCoordinates}
                hasForm
                readOnly={isFetchingCoordinates}
                isResponsive
                autoComplete="off"
                autoFocus={false}
                label={t('stores.form.label.zipCode')}
                labelWidth={labelWidth}
                placeholder={t('stores.form.placeholder.zipCode')}
                required
                {...field}
              />
            );
          }}
        />

        <FormField
          control={form.control}
          name="latitude"
          render={({ field }) => {
            return (
              <Input
                readOnly={true}
                hasForm
                autoComplete="off"
                autoFocus={false}
                isResponsive
                label={t('stores.form.label.latitude')}
                labelWidth={labelWidth}
                placeholder={t('stores.form.placeholder.latitude')}
                required
                {...field}
              />
            );
          }}
        />

        <FormField
          control={form.control}
          name="longitude"
          render={({ field }) => {
            return (
              <Input
                readOnly={true}
                hasForm
                autoComplete="off"
                autoFocus={false}
                isResponsive
                label={t('stores.form.label.longitude')}
                labelWidth={labelWidth}
                placeholder={t('stores.form.placeholder.longitude')}
                required
                {...field}
              />
            );
          }}
        />

        <FormField
          control={form.control}
          name="phone"
          render={({ field }) => {
            return (
              <Input
                disabled={isLoading}
                hasForm
                isResponsive
                autoComplete="off"
                autoFocus={false}
                label={t('stores.form.label.phone')}
                labelWidth={labelWidth}
                placeholder={t('stores.form.placeholder.phone')}
                required
                {...field}
              />
            );
          }}
        />

        <div className="pt-3 space-x-2 flex  w-full">
          <Button
            isLoading={isLoading}
            type="submit"
            size="sm"
            className="w-full"
          >
            {t('global.save')}
          </Button>
        </div>
      </form>
    </Form>
  );
};

export { FormSetting };
