import React from 'react';
import { FormSetting } from './_components/form-setting';
import { getStoreByUserId } from '@/lib/get-store';
import { auth } from '@clerk/nextjs/server';
import { Header } from '@/components/header';
import { ActionButton } from './_components/action-button';

interface SettingPageProps {
  params: { storeId: string };
}

async function Page({ params }: SettingPageProps) {
  const { userId } = auth();
  const store = await getStoreByUserId(userId!, params.storeId);

  return (
    <div className="flex flex-col justify-start items-start">
      <Header
        title="Settings"
        description="Manage Store Preferences"
        actionButton={<ActionButton data={store ?? null} />}
      />
      <FormSetting initData={store ?? null} />
    </div>
  );
}

export default Page;
