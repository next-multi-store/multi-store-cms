import { Billboard } from '@/schemas/z-schema';
import { create } from 'zustand';
import { IDataParams, getData } from '../_utils/get-data';

interface PageState {
  isFirstPage: boolean;
  isLastPage: boolean;
  isNextPage: boolean;
  isPrevPage: boolean;
}

interface TableStateProps {
  records: Billboard[];
  page: PageState;
  limit: number;
  isLoading: boolean;
  pageCount: number;
  error: string | null;
  setRecords: (records: Billboard[]) => void;
  setPage: (page: PageState) => void;
  setLimit: (limit: number) => void;
  setIsLoading: (isLoading: boolean) => void;
  setError: (error: string) => void;
  refreshRecords: ({
    pLimit,
    pStoreId,
    pPage,
  }: {
    pLimit?: number;
    pPage?: PageState;
    pStoreId: string;
  }) => Promise<void>;
}

const useTable = create<TableStateProps>((set) => ({
  records: [],
  page: {
    isFirstPage: false,
    isLastPage: false,
    isNextPage: false,
    isPrevPage: false,
  },
  limit: 10,
  isLoading: false,
  pageCount: 0,
  lastPage: 'false',
  error: null,
  setRecords: (records) => set({ records }),
  setPage: (page: PageState) => set({ page }),
  setLimit: (limit) => set({ limit }),
  setIsLoading: (isLoading) => set({ isLoading }),
  setError: (error) => set({ error }),
  refreshRecords: async ({ pLimit, pPage, pStoreId }) => {
    try {
      set({ isLoading: true });
      let payload: IDataParams = {
        limit: pLimit,
        storeId: pStoreId,
      };

      if (pPage?.isFirstPage) {
        payload = {
          ...payload,
          isFirstPage: pPage.isFirstPage,
        };
      }

      if (pPage?.isNextPage) {
        payload = {
          ...payload,
          isNextPage: pPage.isNextPage,
        };
      }

      if (pPage?.isLastPage) {
        payload = {
          ...payload,
          isLastPage: pPage.isLastPage,
        };
      }

      if (pPage?.isPrevPage) {
        payload = {
          ...payload,
          isPrevPage: pPage.isPrevPage,
        };
      }

      const { records, pageCount, error } = await getData(payload);

      set((state) => ({
        ...state,
        records,
        pageCount,
        limit: pLimit,
      }));

      if (error) {
        throw new Error('error while fetching');
      }
    } catch (error) {
      set((state) => ({ ...state, error: 'error while fetching' }));
    } finally {
      set((state) => ({
        ...state,
        isLoading: false,
      }));
    }
  },
}));

export { useTable, type PageState };
