import { ErrorWithStatus, ms } from '@/lib/fetch';
import { Billboard, billboardSchema } from '@/schemas/z-schema';
import { RedirectType, redirect } from 'next/navigation';

export interface IDataParams {
  storeId: string;
  limit?: number;
  isFirstPage?: boolean;
  isLastPage?: boolean;
  isNextPage?: boolean;
  isPrevPage?: boolean;
}

interface DataReturn {
  records: Billboard[];
  pageCount?: number;
  offset?: number;
  error?: { status: number; message: string };
}

interface ApiResponse<T> {
  code: number;
  message: string;
  data: {
    lastVisibleId: string;
    pageCount: number;
    currentOffset: number;
    data: T[];
  };
}

const getData = async ({
  storeId,
  limit,
  isFirstPage,
  isLastPage,
  isNextPage,
  isPrevPage,
}: IDataParams): Promise<DataReturn> => {
  const records: Billboard[] = [];
  try {
    const params: Record<string, string> = {};
    let url = `/api/${storeId}/billboards`;

    if (limit) params.limit = limit.toString();
    if (isFirstPage) params.first_page = 'true';
    if (isLastPage) params.last_page = 'true';
    if (isNextPage) params.next_page = 'true';
    if (isPrevPage) params.prev_page = 'true';

    const searchParams = new URLSearchParams(params);
    url += `?${searchParams.toString()}`;

    const response: ApiResponse<Billboard> = await ms.get(url);
    const { data } = response;
    const { data: billboard, pageCount, currentOffset: offset } = data;

    const validatedData = billboardSchema.array().parse(billboard);
    validatedData.map((item) => {
      return records.push(item);
    });

    return { records, pageCount, offset };
  } catch (error) {
    const err = error as ErrorWithStatus;
    if (err.status === 403) {
      return redirect('/', RedirectType.replace);
    }
    return {
      records: [],
      error: { status: err.status, message: err.error },
    };
  }
};

export { getData };
