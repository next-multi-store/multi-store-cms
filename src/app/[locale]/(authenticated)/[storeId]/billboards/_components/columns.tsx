'use client';

import type { ColumnDef } from '@tanstack/react-table';

import {
  DataTableRowAction,
  type Handle,
} from '@/components/data-table-row-action';

import { Header } from './table';
import { Billboard } from '@/schemas/z-schema';
import { DialogVariant, DialogVariantValues } from '@/schemas/dialog';
import { FallbackImage } from '@/components/fallback-image';

interface ColumnParams {
  header: Header;
  onHandleView: (data: Billboard, type: DialogVariant) => void;
  onHandleEdit: (data: Billboard, type: DialogVariant) => void;
  onHandleDelete: (data: Billboard, type: DialogVariant) => void;
}

export const createColumns = ({
  header,
  onHandleEdit,
  onHandleDelete,
}: ColumnParams): ColumnDef<Billboard>[] => {
  return [
    {
      accessorKey: 'no',
      header: header.no,
    },
    {
      accessorKey: 'label',
      header: header.label,
    },
    {
      accessorKey: 'imageUrl',
      header: header.image,
      cell: ({ row }) => {
        const imageUrl = row.getValue('imageUrl') as string;
        const label = row.getValue('label') as string;

        return (
          <FallbackImage src={imageUrl} alt={label} width={130} height={87} />
        );
      },
    },

    {
      id: 'actions',
      cell: ({ row }) => {
        const handles: Handle[] = [
          {
            name: 'update',
            callback: () =>
              onHandleEdit(row.original, DialogVariantValues.update),
          },
          {
            name: 'delete',
            callback: () =>
              onHandleDelete(row.original, DialogVariantValues.delete),
          },
        ];

        return <DataTableRowAction handles={handles} row={row.original} />;
      },
    },
  ];
};
