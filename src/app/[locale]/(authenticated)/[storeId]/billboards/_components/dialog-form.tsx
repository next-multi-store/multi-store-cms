'use client';
import { Billboard } from '@/schemas/z-schema';
import { useRef, useState } from 'react';
import { useTable } from '../_hooks/use-table';
import {
  Dialog,
  DialogClose,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
} from '@/components/ui/dialog';

import { Button } from '@/components/ui/button';
import { useTranslations } from 'next-intl';
import { Form } from './form';

interface DialogFormProps {
  title: string;
  isOpen: boolean;
  initData?: Billboard;
  onHandleCloseDialog: () => void;
  storeId: string;
}
const DialogForm = ({
  title,
  isOpen,
  initData,
  onHandleCloseDialog,
  storeId,
}: DialogFormProps) => {
  const formRef = useRef<HTMLFormElement>(null);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const { limit, refreshRecords } = useTable();
  const t = useTranslations();

  const handleSubmit = () => {
    if (formRef.current) {
      formRef.current.dispatchEvent(
        new Event('submit', { bubbles: true, cancelable: true }),
      );
    }
  };

  const handleSuccess = () => {
    onHandleCloseDialog();
    refreshRecords({ pLimit: limit, pStoreId: storeId });
  };

  return (
    <Dialog open={isOpen} onOpenChange={onHandleCloseDialog}>
      <DialogContent
        className="w-11/12 overflow-y-auto rounded-lg sm:h-max sm:max-w-lg"
        onInteractOutside={(e) => {
          e.preventDefault();
        }}
      >
        <DialogHeader>
          <DialogTitle>{title}</DialogTitle>
        </DialogHeader>
        <Form
          ref={formRef}
          initData={initData}
          isLoading={isLoading}
          setLoading={setIsLoading}
          storeId={storeId}
          onSuccess={handleSuccess}
        />
        <DialogFooter className="gap-2">
          <DialogClose asChild>
            <Button type="button" variant="outline">
              {t('global.cancel')}
            </Button>
          </DialogClose>

          <Button
            type="submit"
            isLoading={isLoading}
            className="!m-0"
            onClick={handleSubmit}
          >
            {t('global.save')}
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

export { DialogForm };
