'use client';

import { useDialogWithActions } from '@/hooks/use-dialog';
import { DialogVariant, DialogVariantValues } from '@/schemas/dialog';
import { Billboard } from '@/schemas/z-schema';
import { createColumns } from './columns';
import { DataTable, PageStatus } from '@/components/ui/data-table';

import { useEffect, useState } from 'react';
import { DialogForm } from './dialog-form';
import { useTranslations } from 'next-intl';
import { DeleteDialog } from '@/components/delete-dialog';
import axios from 'axios';
import { useToast } from '@/hooks/use-toast';
import { useTable } from '../_hooks/use-table';

export type Header = {
  no: string;
  label: string;
  image: string;
};

interface TableProps {
  storeId: string;
}

const Table = ({ storeId }: TableProps) => {
  const {
    records,
    limit,
    pageCount,
    page,
    isLoading: isFetching,
    refreshRecords,
    setPage,
    setLimit,
  } = useTable();
  const { selectedDialog, handleAction, selectedData } =
    useDialogWithActions<Billboard>();
  const t = useTranslations();
  const [isDeleting, setIsDeleting] = useState<boolean>(false);
  const { toast } = useToast();

  useEffect(() => {
    refreshRecords({
      pLimit: limit,
      pStoreId: storeId,
      pPage: page,
    });
  }, [refreshRecords, page, limit]);

  const isEditDialogOpen = selectedDialog === DialogVariantValues.update;
  const isDeleteDialogOpen = selectedDialog === DialogVariantValues.delete;

  const handleDialog = (data: Billboard, type: DialogVariant) => {
    handleAction(data, type);
  };

  const handleSubmitDelete = async (data: Billboard) => {
    try {
      setIsDeleting(true);
      await axios.delete(`/api/${storeId}/billboards/${data.id}`);

      toast({
        title: 'Success',
        description: t('global.dialog.success.delete', {
          name: t('billboards.title'),
        }),
        variant: 'success',
      });

      handleAction(undefined, undefined);
      refreshRecords({ pLimit: limit, pStoreId: storeId });
    } catch (error) {
      toast({
        title: t('global.error'),
        description: t('global.somethingWrong'),
        variant: 'error',
      });
    } finally {
      setIsDeleting(false);
    }
  };

  const header: Header = {
    no: t('billboards.table.no'),
    label: t('billboards.table.label'),
    image: t('billboards.table.image'),
  };

  return (
    <>
      <DataTable
        columns={createColumns({
          header,
          onHandleView: handleDialog,
          onHandleEdit: handleDialog,
          onHandleDelete: handleDialog,
        })}
        data={records}
        hasPagination
        limit={limit}
        isLoading={isFetching}
        pageCount={pageCount}
        onHandleSetLimit={setLimit}
        onHandleSetPage={(status: PageStatus) => {
          switch (status) {
            case 'first':
              setPage({
                isFirstPage: true,
                isLastPage: false,
                isNextPage: false,
                isPrevPage: false,
              });
              break;
            case 'last':
              setPage({
                isFirstPage: false,
                isLastPage: true,
                isNextPage: false,
                isPrevPage: false,
              });
              break;
            case 'next':
              setPage({
                isFirstPage: false,
                isLastPage: false,
                isNextPage: true,
                isPrevPage: false,
              });
              break;
            case 'prev':
              setPage({
                isFirstPage: false,
                isLastPage: false,
                isNextPage: false,
                isPrevPage: true,
              });
              break;
            default:
              setPage({
                isFirstPage: false,
                isLastPage: false,
                isNextPage: false,
                isPrevPage: false,
              });
              break;
          }
        }}
      />

      {isEditDialogOpen && selectedData && (
        <DialogForm
          isOpen={isEditDialogOpen}
          onHandleCloseDialog={() => handleAction(undefined, undefined)}
          storeId={storeId}
          title={t('global.dialog.titleEdit', { name: t('billboards.title') })}
          initData={selectedData}
        />
      )}

      {isDeleteDialogOpen && selectedData && (
        <DeleteDialog<Billboard>
          isLoading={isDeleting}
          isOpenDialog={isDeleteDialogOpen}
          selectedData={selectedData}
          onHandleSubmitDelete={(billboard: Billboard) =>
            handleSubmitDelete(billboard)
          }
          onCloseDialog={() => handleAction(undefined, undefined)}
          title={selectedData.label}
        />
      )}
    </>
  );
};

export { Table };
