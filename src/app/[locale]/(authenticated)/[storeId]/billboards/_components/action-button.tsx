'use client';
import { Button } from '@/components/ui/button';
import { Plus } from 'lucide-react';
import React, { useState } from 'react';

import { DialogForm } from './dialog-form';
import { useTranslations } from 'next-intl';

interface ActionButtonProps {
  storeId: string;
}

const ActionButton = ({ storeId }: ActionButtonProps) => {
  const [isDialogOpen, setIsDialogOpen] = useState<boolean>(false);
  const t = useTranslations();

  return (
    <>
      <Button
        size={'default'}
        type="button"
        className="w-full"
        onClick={() => setIsDialogOpen(true)}
      >
        <Plus className="size-4 min-h-4 min-w-4 mr-2" /> {t('global.addNew')}
      </Button>
      {isDialogOpen && (
        <DialogForm
          isOpen={isDialogOpen}
          onHandleCloseDialog={() => setIsDialogOpen(false)}
          storeId={storeId}
          title={t('global.dialog.titleNew', { name: t('billboards.title') })}
        />
      )}
    </>
  );
};

export { ActionButton };
