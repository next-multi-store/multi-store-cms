import { Header } from '@/components/header';
import { ActionButton } from './_components/action-button';
import { Table } from './_components/table';
import { getTranslations } from 'next-intl/server';

interface BillboardPageProps {
  params: { locale: string; storeId: string };
}

export const generateMetadata = async ({ params }: BillboardPageProps) => {
  const t = await getTranslations({
    locale: params.locale,
    namespace: 'billboards',
  });

  return {
    title: t('metaTitle'),
    description: t('metaDescription'),
  };
};

const Page = async ({ params }: Readonly<BillboardPageProps>) => {
  const { storeId, locale } = params;
  const t = await getTranslations({
    locale: locale,
    namespace: 'billboards',
  });

  return (
    <>
      <div className="flex flex-col justify-start items-start mb-7">
        <Header
          title={t('title')}
          description={t('description')}
          actionButton={<ActionButton storeId={storeId} />}
        />
      </div>
      <Table storeId={storeId} />
    </>
  );
};

export default Page;
