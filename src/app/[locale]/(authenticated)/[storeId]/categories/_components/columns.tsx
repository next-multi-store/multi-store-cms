'use client';

import type { ColumnDef } from '@tanstack/react-table';

import {
  DataTableRowAction,
  type Handle,
} from '@/components/data-table-row-action';

import { Header } from './table';
import { Category } from '@/schemas/z-schema';
import { DialogVariant, DialogVariantValues } from '@/schemas/dialog';
import { FallbackImage } from '@/components/fallback-image';

interface ColumnParams {
  header: Header;
  onHandleView: (data: Category, type: DialogVariant) => void;
  onHandleEdit: (data: Category, type: DialogVariant) => void;
  onHandleDelete: (data: Category, type: DialogVariant) => void;
}

export const createColumns = ({
  header,
  onHandleEdit,
  onHandleDelete,
}: ColumnParams): ColumnDef<Category>[] => {
  return [
    {
      accessorKey: 'no',
      header: header.no,
    },
    {
      accessorKey: 'name',
      header: header.name,
    },
    {
      accessorKey: 'description',
      header: header.description,
    },
    {
      accessorKey: 'imageUrl',
      header: header.image,
      cell: ({ row }) => {
        const imageUrl = row.getValue('imageUrl') as string;
        const alt = row.getValue('name') as string;

        return (
          <FallbackImage src={imageUrl} alt={alt} width={130} height={87} />
        );
      },
    },

    {
      id: 'actions',
      cell: ({ row }) => {
        const handles: Handle[] = [
          {
            name: 'update',
            callback: () =>
              onHandleEdit(row.original, DialogVariantValues.update),
          },
          {
            name: 'delete',
            callback: () =>
              onHandleDelete(row.original, DialogVariantValues.delete),
          },
        ];

        return <DataTableRowAction handles={handles} row={row.original} />;
      },
    },
  ];
};
