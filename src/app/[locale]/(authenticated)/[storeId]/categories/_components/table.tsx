'use client';

import { useDialogWithActions } from '@/hooks/use-dialog';
import { DialogVariant, DialogVariantValues } from '@/schemas/dialog';
import { Billboard, Category } from '@/schemas/z-schema';
import { createColumns } from './columns';
import { DataTable, PageStatus } from '@/components/ui/data-table';
import { useEffect, useState } from 'react';
import { DialogForm } from './dialog-form';
import { useTranslations } from 'next-intl';
import { DeleteDialog } from '@/components/delete-dialog';
import axios from 'axios';
import { useToast } from '@/hooks/use-toast';
import { useTable } from '../_hooks/use-table';

type Header = {
  no: string;
  name: string;
  description: string;
  image: string;
};

interface TableProps {
  storeId: string;
}

const Table = ({ storeId }: TableProps) => {
  const {
    records,
    limit,
    pageCount,
    page,
    isLoading: isFetching,
    refreshRecords,
    setPage,
    setLimit,
  } = useTable();
  const { selectedDialog, handleAction, selectedData } =
    useDialogWithActions<Category>();
  const t = useTranslations();
  const [isDeleting, setIsDeleting] = useState<boolean>(false);
  const { toast } = useToast();

  useEffect(() => {
    refreshRecords({
      pLimit: limit,
      pStoreId: storeId,
      pPage: page,
    });
  }, [refreshRecords, page, limit]);

  const isEditDialogOpen = selectedDialog === DialogVariantValues.update;
  const isDeleteDialogOpen = selectedDialog === DialogVariantValues.delete;

  const handleDialog = (data: Category, type: DialogVariant) => {
    handleAction(data, type);
  };

  const handleSubmitDelete = async (data: Category) => {
    try {
      setIsDeleting(true);
      await axios.delete(`/api/${storeId}/categories/${data.id}`);

      toast({
        title: 'Success',
        description: t('global.dialog.success.delete', {
          name: t('categories.title'),
        }),
        variant: 'success',
      });

      handleAction(undefined, undefined);
      refreshRecords({ pLimit: limit, pStoreId: storeId });
    } catch (error) {
      toast({
        title: t('global.error'),
        description: t('global.somethingWrong'),
        variant: 'error',
      });
    } finally {
      setIsDeleting(false);
    }
  };

  const header: Header = {
    no: t('categories.table.no'),
    name: t('categories.table.name'),
    description: t('categories.table.description'),
    image: t('categories.table.image'),
  };

  return (
    <>
      <DataTable
        columns={createColumns({
          header,
          onHandleView: handleDialog,
          onHandleEdit: handleDialog,
          onHandleDelete: handleDialog,
        })}
        data={records}
        hasPagination
        limit={limit}
        isLoading={isFetching}
        pageCount={pageCount}
        onHandleSetLimit={setLimit}
        onHandleSetPage={(status: PageStatus) => {
          switch (status) {
            case 'first':
              setPage({
                isFirstPage: true,
                isLastPage: false,
                isNextPage: false,
                isPrevPage: false,
              });
              break;
            case 'last':
              setPage({
                isFirstPage: false,
                isLastPage: true,
                isNextPage: false,
                isPrevPage: false,
              });
              break;
            case 'next':
              setPage({
                isFirstPage: false,
                isLastPage: false,
                isNextPage: true,
                isPrevPage: false,
              });
              break;
            case 'prev':
              setPage({
                isFirstPage: false,
                isLastPage: false,
                isNextPage: false,
                isPrevPage: true,
              });
              break;
            default:
              setPage({
                isFirstPage: false,
                isLastPage: false,
                isNextPage: false,
                isPrevPage: false,
              });
              break;
          }
        }}
      />

      {isEditDialogOpen && selectedData && (
        <DialogForm
          isOpen={isEditDialogOpen}
          onHandleCloseDialog={() => handleAction(undefined, undefined)}
          storeId={storeId}
          title={t('global.dialog.titleEdit', { name: t('categories.title') })}
          initData={selectedData}
        />
      )}

      {isDeleteDialogOpen && selectedData && (
        <DeleteDialog<Category>
          isLoading={isDeleting}
          isOpenDialog={isDeleteDialogOpen}
          selectedData={selectedData}
          onHandleSubmitDelete={(data: Category) => handleSubmitDelete(data)}
          onCloseDialog={() => handleAction(undefined, undefined)}
          title={selectedData.name}
        />
      )}
    </>
  );
};

export { Table, type Header };
