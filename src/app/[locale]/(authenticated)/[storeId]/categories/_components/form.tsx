'use client';
import { Upload } from '@/components/upload';
import { Form as UIForm, FormField } from '@/components/ui/form';
import { Input } from '@/components/ui/input';
import { Category, categorySchema } from '@/schemas/z-schema';
import { zodResolver } from '@hookform/resolvers/zod';
import { forwardRef, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useUpload } from '@/hooks/use-upload';
import { IMAGE_TYPES } from '@/schemas/common';
import { useToast } from '@/hooks/use-toast';
import axios from 'axios';
import { useTranslations } from 'next-intl';
import { Textarea } from '@/components/ui/textarea';

interface FormProps {
  initData?: Category;
  isLoading: boolean;
  storeId: string;
  setLoading: (isLoading: boolean) => void;
  onSuccess: () => void;
}

const Form = forwardRef<HTMLFormElement, FormProps>(
  ({ initData, setLoading, isLoading, storeId, onSuccess }, ref) => {
    const t = useTranslations();
    const { toast } = useToast();
    const {
      previews,
      setPreviewsByFileUrls,
      handleSelect,
      handleRemove,
      upload,
      fileInputRef,
      handleResetUpload,
    } = useUpload({
      maxSize: 1024,
      accepts: IMAGE_TYPES,
    });

    const form = useForm<Category>({
      resolver: zodResolver(categorySchema),
      defaultValues: initData ?? {
        name: undefined,
        description: undefined,
        imageUrl: undefined,
      },
    });

    useEffect(() => {
      handleResetUpload();
      if (initData) {
        form.reset({
          id: initData.id,
          name: initData.name,
          description: initData.description,
          imageUrl: initData.imageUrl,
        });

        if (initData.imageUrl) {
          setPreviewsByFileUrls([initData.imageUrl]);
        }
      }
    }, [initData]);

    const handleSubmit = async () => {
      setLoading(true);
      try {
        const urls = await upload('categories');
        const selectedId = initData?.id;
        form.setValue('imageUrl', urls[0] ?? '');
        const isValid = await form.trigger();
        if (isValid) {
          const formValue = form.getValues();
          const payload = {
            name: formValue.name,
            imageUrl: formValue.imageUrl,
            description: formValue.description,
          };

          if (!selectedId) {
            await axios.post(`/api/${storeId}/categories`, payload);
          } else {
            await axios.patch(
              `/api/${storeId}/categories/${selectedId}`,
              payload,
            );
          }
        }

        toast({
          title: 'Success',
          description: !selectedId
            ? t('global.dialog.success.insertNew', {
                name: t('categories.title'),
              })
            : t('global.dialog.success.edit', {
                name: t('categories.title'),
              }),
          variant: 'success',
        });
        onSuccess();
      } catch (error) {
        toast({
          title: t('global.error'),
          description: t('global.somethingWrong'),
          variant: 'error',
        });
      } finally {
        setLoading(false);
      }
    };

    return (
      <UIForm {...form}>
        <form
          ref={ref}
          onSubmit={form.handleSubmit(handleSubmit)}
          className="space-y-1 gap-3 flex flex-col md:flex-col"
        >
          <FormField
            control={form.control}
            name="name"
            render={({ field }) => (
              <Input
                hasForm
                isResponsive
                label={t('categories.form.label.name')}
                labelWidth={100}
                placeholder={t('categories.form.placeholder.name')}
                required
                disabled={isLoading}
                {...field}
              />
            )}
          />

          <FormField
            control={form.control}
            name="description"
            render={({ field }) => (
              <Textarea
                hasForm
                isResponsive
                label={t('categories.form.label.description')}
                labelWidth={100}
                placeholder={t('categories.form.placeholder.description')}
                required
                disabled={isLoading}
                {...field}
              />
            )}
          />

          <FormField
            control={form.control}
            name="imageUrl"
            render={({ field }) => {
              return (
                <Upload
                  ref={fileInputRef}
                  accept={IMAGE_TYPES.join(',')}
                  isResponsive
                  limit={1}
                  label={t('categories.form.label.image')}
                  labelWidth={100}
                  onChange={(event) => {
                    handleSelect(event, {
                      callback: field.onChange,
                      isSingleFile: true,
                    });
                  }}
                  onRemove={(name) => {
                    handleRemove(name, {
                      callback: field.onChange,
                      isSingleFile: true,
                    });
                  }}
                  placeholder={t('global.upload')}
                  previews={previews}
                  disabled={isLoading}
                  required
                />
              );
            }}
          />
        </form>
      </UIForm>
    );
  },
);

Form.displayName = 'Form';
export { Form };
