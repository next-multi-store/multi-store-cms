'use client';
import { Upload } from '@/components/upload';
import { Form as UIForm, FormField } from '@/components/ui/form';
import { Input } from '@/components/ui/input';
import { Product, productSchema } from '@/schemas/z-schema';
import { zodResolver } from '@hookform/resolvers/zod';
import { forwardRef, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useUpload } from '@/hooks/use-upload';
import { IMAGE_TYPES } from '@/schemas/common';
import { useToast } from '@/hooks/use-toast';
import axios from 'axios';
import { useTranslations } from 'next-intl';
import { Textarea } from '@/components/ui/textarea';
import { Switch } from '@/components/ui/switch';

interface FormProps {
  initData?: Product;
  isLoading: boolean;
  storeId: string;
  setLoading: (isLoading: boolean) => void;
  onSuccess: () => void;
}

const Form = forwardRef<HTMLFormElement, FormProps>(
  ({ initData, setLoading, isLoading, storeId, onSuccess }, ref) => {
    const t = useTranslations();
    const labelWidth = 100;
    const { toast } = useToast();
    const {
      previews,
      setPreviewsByFileUrls,
      handleSelect,
      handleRemove,
      upload,
      fileInputRef,
      handleResetUpload,
    } = useUpload({
      maxSize: 1024,
      accepts: IMAGE_TYPES,
    });

    const form = useForm<Product>({
      resolver: zodResolver(productSchema),
      defaultValues: initData ?? {
        name: undefined,
        description: undefined,
        imageUrl: undefined,
        categoryId: undefined,
        price: undefined,
      },
    });

    useEffect(() => {
      handleResetUpload();
      if (initData) {
        form.reset({
          id: initData.id,
          name: initData.name,
          description: initData.description,
          imageUrl: initData.imageUrl,
          categoryId: initData.categoryId,
          price: initData.price,
        });

        if (initData.imageUrl) {
          setPreviewsByFileUrls([initData.imageUrl]);
        }
      }
    }, [initData]);

    const handleSubmit = async () => {
      setLoading(true);
      try {
        const urls = await upload('products');
        const selectedId = initData?.id;
        form.setValue('imageUrl', urls[0] ?? '');
        const isValid = await form.trigger();
        if (isValid) {
          const formValue = form.getValues();
          const payload = {
            name: formValue.name,
            imageUrl: formValue.imageUrl,
            description: formValue.description,
            categoryId: formValue.categoryId,
            price: formValue.price,
          };

          if (!selectedId) {
            await axios.post(`/api/${storeId}/products`, payload);
          } else {
            await axios.patch(
              `/api/${storeId}/products/${selectedId}`,
              payload,
            );
          }
        }

        toast({
          title: 'Success',
          description: !selectedId
            ? t('global.dialog.success.insertNew', {
                name: t('products.title'),
              })
            : t('global.dialog.success.edit', {
                name: t('products.title'),
              }),
          variant: 'success',
        });
        onSuccess();
      } catch (error) {
        toast({
          title: t('global.error'),
          description: t('global.somethingWrong'),
          variant: 'error',
        });
      } finally {
        setLoading(false);
      }
    };

    return (
      <UIForm {...form}>
        <form
          ref={ref}
          onSubmit={form.handleSubmit(handleSubmit)}
          className="space-y-1 gap-3 flex flex-col md:flex-col"
        >
          <FormField
            control={form.control}
            name="name"
            render={({ field }) => (
              <Input
                hasForm
                isResponsive
                label={t('products.form.label.name')}
                labelWidth={labelWidth}
                placeholder={t('products.form.placeholder.name')}
                required
                disabled={isLoading}
                {...field}
              />
            )}
          />

          <FormField
            control={form.control}
            name="name"
            render={({ field }) => (
              <Input
                hasForm
                isResponsive
                label={t('products.form.label.category')}
                labelWidth={labelWidth}
                placeholder={t('products.form.placeholder.category')}
                required
                disabled={isLoading}
                {...field}
              />
            )}
          />

          <FormField
            control={form.control}
            name="description"
            render={({ field }) => (
              <Textarea
                hasForm
                isResponsive
                label={t('products.form.label.description')}
                labelWidth={labelWidth}
                placeholder={t('products.form.placeholder.description')}
                required
                disabled={isLoading}
                {...field}
              />
            )}
          />

          <FormField
            control={form.control}
            name="imageUrl"
            render={({ field }) => {
              return (
                <Upload
                  ref={fileInputRef}
                  accept={IMAGE_TYPES.join(',')}
                  isResponsive
                  limit={1}
                  label={t('categories.form.label.image')}
                  labelWidth={labelWidth}
                  onChange={(event) => {
                    handleSelect(event, {
                      callback: field.onChange,
                      isSingleFile: true,
                    });
                  }}
                  onRemove={(name) => {
                    handleRemove(name, {
                      callback: field.onChange,
                      isSingleFile: true,
                    });
                  }}
                  placeholder={t('global.upload')}
                  previews={previews}
                  disabled={isLoading}
                  required
                />
              );
            }}
          />

          <FormField
            control={form.control}
            name="price"
            render={({ field }) => (
              <Input
                hasForm
                isResponsive
                pre="Rp."
                label={t('products.form.label.price')}
                labelWidth={labelWidth}
                placeholder={t('products.form.placeholder.price')}
                required
                type="number"
                disabled={isLoading}
                {...field}
              />
            )}
          />

          <FormField
            control={form.control}
            name="status"
            render={({ field }) => (
              <Switch
                checked={field.value}
                hasForm
                label={t('products.form.label.status')}
                labelActive={t('global.active')}
                labelInactive={t('global.nonActive')}
                labelWidth={labelWidth}
                onCheckedChange={field.onChange}
              />
            )}
          />
        </form>
      </UIForm>
    );
  },
);

Form.displayName = 'Form';
export { Form };
