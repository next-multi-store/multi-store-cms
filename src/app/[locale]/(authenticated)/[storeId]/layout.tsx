import { Navbar } from '@/components/navbar';
import { auth } from '@clerk/nextjs/server';
import { redirect } from 'next/navigation';
import { getStoreByUserId } from '@/lib/get-store';
import Sidebar from '@/components/sidebar';

interface AuthenticatedLayoutProps {
  children: Readonly<React.ReactNode>;
  params: { storeId: string };
}

export default async function Layout({
  children,
  params,
}: AuthenticatedLayoutProps) {
  const { userId } = auth();
  const { storeId } = params;

  const selectedStore = await getStoreByUserId(userId!, storeId);

  if (!selectedStore) {
    redirect('/');
  }

  return (
    <div
      className="flex min-h-svh w-full bg-white flex-col relative overflow-hidden"
      style={
        {
          '--expandedSidebarWidth': '250px',
        } as React.CSSProperties
      }
    >
      <Navbar />
      <section className="flex w-full">
        <Sidebar />
        <div className="flex flex-1 flex-col w-full md:ml-[var(--expandedSidebarWidth)] md:w-[calc(100%-var(--expandedSidebarWidth))]">
          <main className="flex flex-1 flex-col gap-4 bg-layout p-5 lg:gap-6 lg:p-6 !bg-white">
            {children}
          </main>
        </div>
      </section>
    </div>
  );
}
