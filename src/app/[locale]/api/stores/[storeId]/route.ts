import { db } from '@/lib/firebase';
import { StoreDB } from '@/schemas/db';
import { phoneIndonesiaRegex } from '@/schemas/z-schema';
import { auth } from '@clerk/nextjs/server';
import {
  deleteDoc,
  doc,
  getDoc,
  serverTimestamp,
  updateDoc,
} from 'firebase/firestore';
import { NextResponse } from 'next/server';

const PATCH = async (
  req: Request,
  { params }: { params: { storeId: string } },
) => {
  try {
    const { userId } = auth();
    const body = await req.json();
    const { storeId } = params;
    const { name, address, city, latitude, longitude, zipCode, phone } = body;

    if (!userId) return new NextResponse('Unauthorized', { status: 400 });

    if (!storeId)
      return new NextResponse('Store id is required', { status: 400 });

    if (!name) return new NextResponse('Name is required', { status: 400 });
    if (!address)
      return new NextResponse('Address is required', { status: 400 });
    if (!city) return new NextResponse('City is required', { status: 400 });
    if (!latitude)
      return new NextResponse('Latitude is required', { status: 400 });
    if (!longitude)
      return new NextResponse('Longitude is required', { status: 400 });
    if (!zipCode)
      return new NextResponse('Zip Code is required', { status: 400 });
    if (!phone)
      return new NextResponse('Phone Code is required', { status: 400 });

    const normalizePhoneNumber = (phoneNumber: string): string => {
      if (phoneIndonesiaRegex.test(phoneNumber)) {
        if (phoneNumber.startsWith('08')) {
          return '+62' + phoneNumber.slice(1);
        } else if (phoneNumber.startsWith('62')) {
          return '+' + phoneNumber;
        } else if (phoneNumber.startsWith('+62')) {
          return phoneNumber;
        }
      }

      return phoneNumber;
    };

    const normalizedPhone = normalizePhoneNumber(phone);

    const docRef = doc(db, 'stores', storeId);
    await updateDoc(docRef, {
      name,
      address,
      city,
      latitude,
      longitude,
      zipCode,
      phone: normalizedPhone,
      updatedAt: serverTimestamp(),
    });

    const store = (await getDoc(docRef)).data() as StoreDB;

    return NextResponse.json({ store }, { status: 200 });
  } catch (err) {
    return new NextResponse('Internal server error', { status: 500 });
  }
};

const DELETE = async (
  _: Request,
  { params }: { params: { storeId: string } },
) => {
  try {
    const { userId } = auth();
    const { storeId } = params;

    if (!userId) return new NextResponse('Unauthorized', { status: 400 });

    if (!storeId)
      return new NextResponse('Store id is required', { status: 400 });

    const docRef = doc(db, 'stores', storeId);
    await deleteDoc(docRef);

    return new NextResponse(null, { status: 204 });
  } catch (err) {
    return new NextResponse('Internal server error', { status: 500 });
  }
};

export { PATCH, DELETE };
