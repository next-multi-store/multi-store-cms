import { db } from '@/lib/firebase';
import { auth } from '@clerk/nextjs/server';
import {
  addDoc,
  collection,
  doc,
  serverTimestamp,
  updateDoc,
} from 'firebase/firestore';
import { NextResponse } from 'next/server';

export const POST = async (req: Request) => {
  try {
    const { userId } = auth();
    const body = await req.json();
    const { name } = body;

    if (!userId) return new NextResponse('Unauthorized', { status: 400 });
    if (!name) return new NextResponse('Name is required', { status: 400 });

    const data = { name, userId, createdAt: serverTimestamp() };
    const storeRef = await addDoc(collection(db, 'stores'), data);
    const id = storeRef.id;
    await updateDoc(doc(db, 'stores', id), {
      ...data,
      id,
      updatedAt: serverTimestamp(),
    });

    return NextResponse.json({ id, ...data }, { status: 201 });
  } catch (err) {
    return new NextResponse('Internal server error', { status: 500 });
  }
};
