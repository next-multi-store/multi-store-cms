import { db } from '@/lib/firebase';
import { Billboard, Category } from '@/schemas/z-schema';
import { auth } from '@clerk/nextjs/server';
import {
  addDoc,
  collection,
  doc,
  getDoc,
  getDocs,
  serverTimestamp,
  updateDoc,
  limit,
  orderBy,
  query,
  getCountFromServer,
  startAfter,
} from 'firebase/firestore';
import { NextResponse } from 'next/server';

const POST = async (
  req: Request,
  { params }: { params: { storeId: string } },
) => {
  try {
    const { userId } = auth();
    const body = await req.json();
    const { storeId } = params;

    if (!userId) {
      return NextResponse.json({ message: 'Un-Authorized' }, { status: 400 });
    }

    const { name, imageUrl, description, categoryId, price, status } = body;

    if (!storeId) {
      return NextResponse.json(
        { message: 'Store id is missing' },
        { status: 400 },
      );
    }

    if (!name) {
      return NextResponse.json({ message: 'name is missing' }, { status: 400 });
    }

    if (!imageUrl) {
      return NextResponse.json(
        { message: 'image is missing' },
        { status: 400 },
      );
    }

    if (!description) {
      return NextResponse.json(
        { message: 'description is missing' },
        { status: 400 },
      );
    }

    if (!categoryId) {
      return NextResponse.json(
        { message: 'category is missing' },
        { status: 400 },
      );
    }

    if (!price) {
      return NextResponse.json(
        { message: 'price is missing' },
        { status: 400 },
      );
    }

    const store = await getDoc(doc(db, 'stores', storeId));

    if (!store.exists()) {
      let storeData = store.data();
      if (storeData?.userId !== userId) {
        return NextResponse.json(
          { message: 'Un-Authorized Access' },
          { status: 400 },
        );
      }
    }

    const payload = {
      name,
      userId,
      description,
      categoryId,
      imageUrl,
      price,
      status,
      createdAt: serverTimestamp(),
    };

    const productRef = await addDoc(
      collection(db, 'stores', storeId, 'products'),
      payload,
    );

    const id = productRef.id;

    await updateDoc(doc(db, 'stores', storeId, 'products', id), {
      ...payload,
      id,
      updatedAt: serverTimestamp(),
    });

    return NextResponse.json(
      { message: 'success', data: { id, ...payload } },
      { status: 200 },
    );
  } catch (error) {
    return NextResponse.json(
      { message: 'internal server error' },
      { status: 500 },
    );
  }
};

const GET = async (
  req: Request,
  { params }: { params: { storeId: string } },
) => {
  const { userId } = auth();
  const { storeId } = params;
  const { searchParams } = new URL(req.url);
  const limitSize = parseInt(searchParams.get('limit') || '10', 10);
  const currentOffset = parseInt(searchParams.get('offset') || '0', 10);
  const goToLastPage = searchParams.get('last_page') === 'true';
  const goToFirstPage = searchParams.get('first_page') === 'true';
  const goToNextPage = searchParams.get('next_page') === 'true';
  const goToPrevPage = searchParams.get('prev_page') === 'true';

  if (!userId) {
    return NextResponse.json({ message: 'Un-Authorized' }, { status: 400 });
  }

  if (!storeId) {
    return NextResponse.json(
      { message: 'Store id is missing' },
      { status: 400 },
    );
  }

  const store = await getDoc(doc(db, 'stores', storeId));
  if (!store.exists()) {
    return NextResponse.json({ message: 'Store not found' }, { status: 404 });
  }

  const storeRef = doc(db, 'stores', storeId);
  const storeData = store.data();

  if (storeData?.userId !== userId) {
    return NextResponse.json(
      { message: 'Un-Authorized Access' },
      { status: 400 },
    );
  }

  let offset = currentOffset;
  const collectionRef = collection(storeRef, 'products');
  const totalSnapshot = await getCountFromServer(collectionRef);
  const totalCount = totalSnapshot.data().count;

  if (goToLastPage) {
    const remainingItems = totalCount % limitSize;
    offset =
      remainingItems === 0
        ? totalCount - limitSize
        : totalCount - remainingItems;
  } else if (goToFirstPage) {
    offset = 0;
  } else if (goToNextPage) {
    offset = Math.min(currentOffset + limitSize, totalCount - limitSize);
  } else if (goToPrevPage) {
    offset = Math.max(currentOffset - limitSize, 0);
  }

  let lastVisibleDoc = null;
  if (offset > 0) {
    const offsetSnapshot = await getDocs(
      query(collectionRef, orderBy('id'), limit(offset)),
    );
    lastVisibleDoc = offsetSnapshot.docs[offset - 1];
  }

  const pQuery = lastVisibleDoc
    ? query(
        collectionRef,
        orderBy('id'),
        startAfter(lastVisibleDoc),
        limit(limitSize),
      )
    : query(collectionRef, orderBy('updatedAt'), limit(limitSize));

  const pSnapshot = await getDocs(pQuery);
  const pData = pSnapshot.docs.map((doc, index) => {
    const data = doc.data();
    return {
      id: doc.id,
      name: data.name,
      description: data.description,
      imageUrl: data.imageUrl,
      updatedAt: data.updatedAt.toDate(),
      no: offset + index + 1,
      userId: data.userId,
    } as Category;
  });

  const lastVisibleDocNext = pSnapshot.docs[pSnapshot.docs.length - 1];
  const lastVisibleIdNext = lastVisibleDocNext ? lastVisibleDocNext.id : null;
  const totalPages = Math.ceil(totalCount / limitSize);

  return NextResponse.json(
    {
      message: 'success',
      data: {
        data: pData,
        lastVisibleId: lastVisibleIdNext,
        currentOffset: offset,
        pageCount: totalPages,
      },
    },
    { status: 200 },
  );
};

export { POST, GET };
