import { env } from '@/env.mjs';
import { db } from '@/lib/firebase';
import { auth } from '@clerk/nextjs/server';
import axios from 'axios';
import { doc, getDoc } from 'firebase/firestore';
import { NextResponse } from 'next/server';

const GET = async (
  req: Request,
  { params }: { params: { storeId: string } },
) => {
  try {
    const { userId } = auth();
    const { storeId } = params;
    const { searchParams } = new URL(req.url);
    const { NEXT_PUBLIC_GOOGLE_MAPS_API_KEY } = env;

    const lat = searchParams.get('lat');
    const lng = searchParams.get('lng');

    if (!userId) {
      return NextResponse.json({ message: 'Un-Authorized' }, { status: 400 });
    }

    if (!storeId) {
      return NextResponse.json(
        { message: 'Store id is missing' },
        { status: 400 },
      );
    }

    const store = await getDoc(doc(db, 'stores', storeId));
    if (!store.exists()) {
      return NextResponse.json({ message: 'Store not found' }, { status: 404 });
    }

    const storeData = store.data();

    if (storeData?.userId !== userId) {
      return NextResponse.json(
        { message: 'Un-Authorized Access' },
        { status: 400 },
      );
    }

    const { data, status } = await axios.get(
      `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=${NEXT_PUBLIC_GOOGLE_MAPS_API_KEY}`,
    );

    if (status !== 200) {
      throw new Error('error while fatching maps');
    }

    const { results } = data;

    if (results && results.length > 0) {
      const addressComponents = results[0].address_components;

      const address = results[0].formatted_address;
      let city = '';
      let zipCode = '';

      for (let component of results[0].address_components) {
        if (component.types.includes('locality')) {
          city = component.long_name;
          break;
        }
      }

      for (let component of addressComponents) {
        if (component.types.includes('postal_code')) {
          zipCode = component.long_name;
          break;
        }
      }

      return NextResponse.json(
        {
          message: 'success',
          data: {
            address,
            city,
            zipCode,
          },
        },
        { status: 200 },
      );
    } else {
      throw new Error('No results found');
    }
  } catch (err) {
    return NextResponse.json(
      {
        message: err,
      },
      { status: 500 },
    );
  }
};

export { GET };
