import { db } from '@/lib/firebase';
import { Billboard } from '@/schemas/z-schema';
import { auth } from '@clerk/nextjs/server';
import {
  addDoc,
  collection,
  doc,
  getDoc,
  getDocs,
  serverTimestamp,
  updateDoc,
  limit,
  orderBy,
  query,
  getCountFromServer,
  startAfter,
} from 'firebase/firestore';
import { NextResponse } from 'next/server';

const POST = async (
  req: Request,
  { params }: { params: { storeId: string } },
) => {
  try {
    const { userId } = auth();
    const body = await req.json();
    const { storeId } = params;

    if (!userId) {
      return NextResponse.json({ message: 'Un-Authorized' }, { status: 400 });
    }

    const { label, imageUrl } = body;

    if (!label) {
      return NextResponse.json(
        { message: 'Billboard label is missing' },
        { status: 400 },
      );
    }

    if (!imageUrl) {
      return NextResponse.json(
        { message: 'Billboard image is missing' },
        { status: 400 },
      );
    }

    if (!storeId) {
      return NextResponse.json(
        { message: 'Store id is missing' },
        { status: 400 },
      );
    }

    const store = await getDoc(doc(db, 'stores', storeId));

    if (!store.exists()) {
      let storeData = store.data();
      if (storeData?.userId !== userId) {
        return NextResponse.json(
          { message: 'Un-Authorized Access' },
          { status: 400 },
        );
      }
    }

    const billboardData = {
      label,
      userId,
      imageUrl: imageUrl,
      createdAt: serverTimestamp(),
    };

    const billboardRef = await addDoc(
      collection(db, 'stores', storeId, 'billboards'),
      billboardData,
    );

    const id = billboardRef.id;

    await updateDoc(doc(db, 'stores', storeId, 'billboards', id), {
      ...billboardData,
      id,
      updatedAt: serverTimestamp(),
    });

    return NextResponse.json(
      { message: 'success', data: { id, ...billboardData } },
      { status: 200 },
    );
  } catch (error) {
    return NextResponse.json(
      { message: 'internal server error' },
      { status: 500 },
    );
  }
};

const GET = async (
  req: Request,
  { params }: { params: { storeId: string } },
) => {
  const { userId } = auth();
  const { storeId } = params;
  const { searchParams } = new URL(req.url);
  const limitSize = parseInt(searchParams.get('limit') || '10', 10);
  const currentOffset = parseInt(searchParams.get('offset') || '0', 10);
  const goToLastPage = searchParams.get('last_page') === 'true';
  const goToFirstPage = searchParams.get('first_page') === 'true';
  const goToNextPage = searchParams.get('next_page') === 'true';
  const goToPrevPage = searchParams.get('prev_page') === 'true';

  if (!userId) {
    return NextResponse.json({ message: 'Un-Authorized' }, { status: 400 });
  }

  if (!storeId) {
    return NextResponse.json(
      { message: 'Store id is missing' },
      { status: 400 },
    );
  }

  const store = await getDoc(doc(db, 'stores', storeId));
  if (!store.exists()) {
    return NextResponse.json({ message: 'Store not found' }, { status: 404 });
  }

  const storeRef = doc(db, 'stores', storeId);
  const storeData = store.data();

  if (storeData?.userId !== userId) {
    return NextResponse.json(
      { message: 'Un-Authorized Access' },
      { status: 400 },
    );
  }

  let offset = currentOffset;
  const billboardCollectionRef = collection(storeRef, 'billboards');
  const totalSnapshot = await getCountFromServer(billboardCollectionRef);
  const totalCount = totalSnapshot.data().count;

  if (goToLastPage) {
    const remainingItems = totalCount % limitSize;
    offset =
      remainingItems === 0
        ? totalCount - limitSize
        : totalCount - remainingItems;
  } else if (goToFirstPage) {
    offset = 0;
  } else if (goToNextPage) {
    offset = Math.min(currentOffset + limitSize, totalCount - limitSize);
  } else if (goToPrevPage) {
    offset = Math.max(currentOffset - limitSize, 0);
  }

  let lastVisibleDoc = null;
  if (offset > 0) {
    const offsetSnapshot = await getDocs(
      query(billboardCollectionRef, orderBy('id'), limit(offset)),
    );
    lastVisibleDoc = offsetSnapshot.docs[offset - 1];
  }

  const billboardQuery = lastVisibleDoc
    ? query(
        billboardCollectionRef,
        orderBy('id'),
        startAfter(lastVisibleDoc),
        limit(limitSize),
      )
    : query(billboardCollectionRef, orderBy('updatedAt'), limit(limitSize));

  const billboardSnapshot = await getDocs(billboardQuery);
  const billboardData = billboardSnapshot.docs.map((doc, index) => {
    const data = doc.data();
    return {
      id: doc.id,
      label: data.label,
      imageUrl: data.imageUrl,
      updatedAt: data.updatedAt.toDate(),
      no: offset + index + 1,
      userId: data.userId,
    } as Billboard;
  });

  const lastVisibleDocNext =
    billboardSnapshot.docs[billboardSnapshot.docs.length - 1];
  const lastVisibleIdNext = lastVisibleDocNext ? lastVisibleDocNext.id : null;
  const totalPages = Math.ceil(totalCount / limitSize);

  return NextResponse.json(
    {
      message: 'success',
      data: {
        data: billboardData,
        lastVisibleId: lastVisibleIdNext,
        currentOffset: offset,
        pageCount: totalPages,
      },
    },
    { status: 200 },
  );
};

export { POST, GET };
