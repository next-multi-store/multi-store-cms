import { db, deleteFileFromFirebase } from '@/lib/firebase';
import { auth } from '@clerk/nextjs/server';
import {
  collection,
  deleteDoc,
  doc,
  getDoc,
  serverTimestamp,
  updateDoc,
} from 'firebase/firestore';
import { NextResponse } from 'next/server';

const DELETE = async (
  _: Request,
  { params }: { params: { storeId: string; id: string } },
) => {
  try {
    const { userId } = auth();
    const { storeId, id: billboardId } = params;

    if (!userId) {
      return NextResponse.json({ message: 'Un-Authorized' }, { status: 400 });
    }

    if (!storeId) {
      return NextResponse.json(
        { message: 'Store id is missing' },
        { status: 400 },
      );
    }

    if (!billboardId) {
      return NextResponse.json(
        { message: 'Billboard id is missing' },
        { status: 400 },
      );
    }

    const storeRef = doc(db, 'stores', storeId);
    const storeDoc = await getDoc(storeRef);

    if (!storeDoc.exists()) {
      return NextResponse.json({ message: 'Store not found' }, { status: 404 });
    }

    const storeData = storeDoc.data();
    if (storeData?.userId !== userId) {
      return NextResponse.json(
        { message: 'Un-Authorized Access' },
        { status: 400 },
      );
    }

    const billboardsRef = collection(storeRef, 'billboards');
    const billboardDocRef = doc(billboardsRef, billboardId);

    const billboardDocSnapshot = await getDoc(billboardDocRef);

    if (!billboardDocSnapshot.exists()) {
      return NextResponse.json(
        { message: 'Billboard data doesnt exists' },
        { status: 400 },
      );
    }

    const imageUrl = billboardDocSnapshot.data().imageUrl;
    await deleteFileFromFirebase(imageUrl);
    await deleteDoc(billboardDocRef);

    return NextResponse.json({ message: 'success' }, { status: 200 });
  } catch (error) {
    return NextResponse.json(
      { message: 'internal server error' },
      { status: 500 },
    );
  }
};

const PATCH = async (
  req: Request,
  { params }: { params: { storeId: string; id: string } },
) => {
  try {
    const { userId } = auth();
    const body = await req.json();
    const { storeId, id: billboardId } = params;

    if (!userId) {
      return NextResponse.json({ message: 'Un-Authorized' }, { status: 400 });
    }

    const { label, imageUrl } = body;

    if (!label) {
      return NextResponse.json(
        { message: 'Billboard label is missing' },
        { status: 400 },
      );
    }

    if (!imageUrl) {
      return NextResponse.json(
        { message: 'Billboard image is missing' },
        { status: 400 },
      );
    }

    if (!storeId) {
      return NextResponse.json(
        { message: 'Store id is missing' },
        { status: 400 },
      );
    }

    if (!billboardId) {
      return NextResponse.json(
        { message: 'Billboard id is missing' },
        { status: 400 },
      );
    }

    const storeRef = doc(db, 'stores', storeId);
    const storeDoc = await getDoc(storeRef);

    if (!storeDoc.exists()) {
      return NextResponse.json({ message: 'Store not found' }, { status: 404 });
    }

    const storeData = storeDoc.data();
    if (storeData?.userId !== userId) {
      return NextResponse.json(
        { message: 'Un-Authorized Access' },
        { status: 400 },
      );
    }

    const billboardsRef = collection(storeRef, 'billboards');
    const billboardDocRef = doc(billboardsRef, billboardId);

    const billboardDocSnapshot = await getDoc(billboardDocRef);

    if (!billboardDocSnapshot.exists()) {
      return NextResponse.json(
        { message: 'Billboard data doesnt exists' },
        { status: 400 },
      );
    }

    const oldImageUrl = billboardDocSnapshot.data().imageUrl;

    if (oldImageUrl && oldImageUrl !== imageUrl) {
      await deleteFileFromFirebase(oldImageUrl);
    }

    const billboardData = {
      label,
      userId,
      imageUrl: imageUrl,
      updatedAt: serverTimestamp(),
    };

    await updateDoc(billboardDocRef, billboardData);

    return NextResponse.json(
      { message: 'success', data: { billboardId, ...billboardData } },
      { status: 200 },
    );
  } catch (error) {
    return NextResponse.json(
      { message: 'internal server error' },
      { status: 500 },
    );
  }
};

export { PATCH, DELETE };
