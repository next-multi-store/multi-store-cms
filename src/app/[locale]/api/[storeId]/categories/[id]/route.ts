import { db, deleteFileFromFirebase } from '@/lib/firebase';
import { auth } from '@clerk/nextjs/server';
import {
  collection,
  deleteDoc,
  doc,
  getDoc,
  serverTimestamp,
  updateDoc,
} from 'firebase/firestore';
import { NextResponse } from 'next/server';

const DELETE = async (
  _: Request,
  { params }: { params: { storeId: string; id: string } },
) => {
  try {
    const { userId } = auth();
    const { storeId, id: categoryId } = params;

    if (!userId) {
      return NextResponse.json({ message: 'Un-Authorized' }, { status: 400 });
    }

    if (!storeId) {
      return NextResponse.json(
        { message: 'Store id is missing' },
        { status: 400 },
      );
    }

    if (!categoryId) {
      return NextResponse.json(
        { message: 'Category id is missing' },
        { status: 400 },
      );
    }

    const storeRef = doc(db, 'stores', storeId);
    const storeSnapshot = await getDoc(storeRef);

    if (!storeSnapshot.exists()) {
      return NextResponse.json({ message: 'Store not found' }, { status: 404 });
    }

    const storeData = storeSnapshot.data();
    if (storeData?.userId !== userId) {
      return NextResponse.json(
        { message: 'Un-Authorized Access' },
        { status: 400 },
      );
    }

    const categoriesRef = collection(storeRef, 'categories');
    const docRef = doc(categoriesRef, categoryId);
    const docSnapshot = await getDoc(docRef);

    if (!docSnapshot.exists()) {
      return NextResponse.json(
        { message: 'Category data doesnt exists' },
        { status: 400 },
      );
    }

    const imageUrl = docSnapshot.data().imageUrl;
    await deleteFileFromFirebase(imageUrl);
    await deleteDoc(docRef);

    return NextResponse.json({ message: 'success' }, { status: 200 });
  } catch (error) {
    return NextResponse.json(
      { message: 'internal server error' },
      { status: 500 },
    );
  }
};

const PATCH = async (
  req: Request,
  { params }: { params: { storeId: string; id: string } },
) => {
  try {
    const { userId } = auth();
    const body = await req.json();
    const { storeId, id: categoryId } = params;

    if (!userId) {
      return NextResponse.json({ message: 'Un-Authorized' }, { status: 400 });
    }

    const { name, imageUrl, description } = body;

    if (!name) {
      return NextResponse.json({ message: 'Name is missing' }, { status: 400 });
    }

    if (!description) {
      return NextResponse.json(
        { message: 'Description is missing' },
        { status: 400 },
      );
    }

    if (!imageUrl) {
      return NextResponse.json(
        { message: 'Billboard image is missing' },
        { status: 400 },
      );
    }

    if (!storeId) {
      return NextResponse.json(
        { message: 'Store id is missing' },
        { status: 400 },
      );
    }

    if (!categoryId) {
      return NextResponse.json(
        { message: 'Category id is missing' },
        { status: 400 },
      );
    }

    const storeRef = doc(db, 'stores', storeId);
    const storeSnapshot = await getDoc(storeRef);

    if (!storeSnapshot.exists()) {
      return NextResponse.json({ message: 'Store not found' }, { status: 404 });
    }

    const storeData = storeSnapshot.data();
    if (storeData?.userId !== userId) {
      return NextResponse.json(
        { message: 'Un-Authorized Access' },
        { status: 400 },
      );
    }

    const categoriesRef = collection(storeRef, 'categories');
    const docRef = doc(categoriesRef, categoryId);
    const docSnapshot = await getDoc(docRef);

    if (!docSnapshot.exists()) {
      return NextResponse.json(
        { message: 'Billboard data doesnt exists' },
        { status: 400 },
      );
    }

    const oldImageUrl = docSnapshot.data().imageUrl;

    if (oldImageUrl && oldImageUrl !== imageUrl) {
      await deleteFileFromFirebase(oldImageUrl);
    }

    const payload = {
      name,
      userId,
      imageUrl: imageUrl,
      updatedAt: serverTimestamp(),
    };

    await updateDoc(docRef, payload);

    return NextResponse.json(
      { message: 'success', data: { categoryId, ...payload } },
      { status: 200 },
    );
  } catch (error) {
    return NextResponse.json(
      { message: 'internal server error' },
      { status: 500 },
    );
  }
};

export { PATCH, DELETE };
