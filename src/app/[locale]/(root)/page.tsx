'use client';
import { useStoreModal } from '@/hooks/use-store-modal';
import { useEffect } from 'react';

export default function Page() {
  const handleOpen = useStoreModal((state) => state.onHandleOpen);
  const isOpen = useStoreModal((state) => state.isOpen);

  useEffect(() => {
    if (!isOpen) {
      handleOpen();
    }
  }, [handleOpen, isOpen]);

  return null;
}
