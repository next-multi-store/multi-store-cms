import { getStore } from '@/lib/get-store';
import { auth } from '@clerk/nextjs/server';
import { redirect } from 'next/navigation';

export default async function Layout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  const { userId } = auth();
  const selectedStore = await getStore(userId!);
  if (selectedStore) redirect(`/${selectedStore.id}/dashboards`);

  return <>{children}</>;
}
