import { createSharedPathnamesNavigation } from 'next-intl/navigation';
import { appConfig } from '@/app.config';

export const { usePathname, useRouter } = createSharedPathnamesNavigation({
  locales: appConfig.i18n.locales,
  localePrefix: appConfig.i18n.localePrefix,
});
