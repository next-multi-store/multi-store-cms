import { getApp, getApps, initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore'; // Import any Firebase services you're using
import {
  deleteObject,
  getDownloadURL,
  getStorage,
  ref,
  uploadBytesResumable,
} from 'firebase/storage';

const firebaseConfig = {
  apiKey: process.env.NEXT_PUBLIC_FIREBASE_API_KEY,
  authDomain: process.env.NEXT_PUBLIC_FIREBASE_AUTH_DOMAIN,
  projectId: process.env.NEXT_PUBLIC_FIREBASE_PROJECT_ID,
  storageBucket: process.env.NEXT_PUBLIC_FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.NEXT_PUBLIC_FIREBASE_MESSAGING_SENDER_ID,
  appId: process.env.NEXT_PUBLIC_FIREBASE_APP_ID,
};

const app = getApps().length > 0 ? getApp() : initializeApp(firebaseConfig);
const db = getFirestore(app);
const storage = getStorage(app);

const deleteFileFromFirebase = async (fileUrl: string) => {
  try {
    const pathParts = fileUrl.split('/o/');
    const path = pathParts[1]?.split('?')[0];

    if (!pathParts || pathParts.length < 2 || !path) {
      throw new Error('Invalid file URL');
    }

    const decodedPath = decodeURIComponent(path);
    const storageRef = ref(storage, decodedPath);
    await deleteObject(storageRef);
  } catch (error) {
    console.error('Error deleting file:', error);
  }
};

const uploadFilesToFirebase = async (files: File[], src: string) => {
  if (files.length === 0) return [];

  const uploadPromises = files.map((file) => {
    return new Promise<string>((resolve, reject) => {
      const uploadTask = uploadBytesResumable(
        ref(storage, `${src}/${Date.now()}-${file.name}`),
        file,
        { contentType: file.type },
      );

      uploadTask.on(
        'state_changed',
        (snapshot) => {
          // You can add progress handling here if needed
        },
        (error) => {
          reject(new Error(error.message));
        },
        () => {
          getDownloadURL(uploadTask.snapshot.ref).then((downloadUrl) => {
            resolve(downloadUrl);
          });
        },
      );
    });
  });

  try {
    const nextFileUrls = await Promise.all(uploadPromises);
    return nextFileUrls;
  } catch (error) {
    return [];
  }
};

export { app, db, storage, deleteFileFromFirebase, uploadFilesToFirebase };
