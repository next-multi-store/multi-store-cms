import type { PreviewProps } from '@/components/upload';
import { IMAGE_EXTENSIONS, IMAGE_TYPES } from '@/schemas/common';

export const validateSize = (file: File, option?: { maxSize?: number }) => {
  const DEFAULT_MAX_SIZE = 512;
  const maxSize = option?.maxSize ?? DEFAULT_MAX_SIZE;
  const isValidSize: boolean = file.size / 1024 < maxSize;

  return { isValidSize };
};

export const validateType = (file: File, option?: { accepts?: string[] }) => {
  const accepts = option?.accepts ?? [];
  const fileTypes = accepts.length > 0 ? accepts : IMAGE_TYPES;
  const isValidType = fileTypes.includes(file.type);
  const isImage = IMAGE_TYPES.includes(file.type);

  return { isValidType, isImage };
};

export const createPreviewsByFileUrls = (fileUrls: string[]) => {
  if (fileUrls.length === 0) return [];

  const previews = fileUrls.map((url) => {
    const urlObj = new URL(url);
    const pathSegments = urlObj.pathname.split('/');
    const lastSegment = pathSegments[pathSegments.length - 1];
    const name = lastSegment ? decodeURIComponent(lastSegment) : '';

    const extension = name.split('.').pop() ?? '';
    const src = url;
    const isImage = IMAGE_EXTENSIONS.includes(extension.toLowerCase());

    return { name, src, isImage };
  });

  return previews;
};

export const createFileSrcsByPreviews = (previews: PreviewProps[]) => {
  if (previews.length === 0) return [];

  const fileSrcs = previews.map((preview) => preview.src);

  return fileSrcs;
};

export const createFileUrlsByPreviews = (previews: PreviewProps[]) => {
  if (previews.length === 0) return [];

  const srcs = createFileSrcsByPreviews(previews);
  const fileUrls = srcs.filter((src) => !src.includes('data:'));

  return fileUrls;
};
