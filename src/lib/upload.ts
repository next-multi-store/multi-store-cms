import type { PreviewProps } from "@/components/upload";
import { IMAGE_EXTENSIONS } from "@/schemas/common";

export function createUploadPreviewsByFileUrls(
  fileUrls: string[],
): PreviewProps[] {
  if (fileUrls.length === 0) return [];

  return fileUrls.map((url) => {
    const parts = url.split("/") ?? [];
    const name = parts[parts.length - 1] ?? "";
    const extension = name?.split(".")?.pop() ?? "";
    const src = url;
    const isImage = IMAGE_EXTENSIONS.includes(extension.toLowerCase());

    return { name, src, isImage };
  });
}
