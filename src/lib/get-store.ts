import { db } from '@/lib/firebase';
import { StoreDB } from '@/schemas/db';
import { collection, getDocs, limit, query, where } from 'firebase/firestore';

async function getStore(userId: string): Promise<StoreDB | null> {
  const storeSnap = await getDocs(
    query(collection(db, 'stores'), where('userId', '==', userId), limit(1)),
  );

  if (!storeSnap.empty) {
    const storeDoc = storeSnap.docs[0];
    const data = storeDoc!.data();
    const createdAt = data.createdAt.toDate();
    const updatedAt = data.updatedAt.toDate();
    return { ...data, createdAt, updatedAt } as StoreDB;
  }

  return null;
}

async function getStoreByUserId(
  userId: string,
  storeId: string,
): Promise<StoreDB | null> {
  const storeSnap = await getDocs(
    query(
      collection(db, 'stores'),
      where('userId', '==', userId),
      where('id', '==', storeId),
      limit(1),
    ),
  );

  if (!storeSnap.empty) {
    const storeDoc = storeSnap.docs[0];
    const data = storeDoc!.data();
    const createdAt = data.createdAt.toDate();
    const updatedAt = data.updatedAt.toDate();

    return { ...data, createdAt, updatedAt } as StoreDB;
  }

  return null;
}

async function getStores(userId: string): Promise<StoreDB[] | null> {
  const storeSnap = await getDocs(
    query(collection(db, 'stores'), where('userId', '==', userId)),
  );

  if (!storeSnap.empty) {
    return storeSnap.docs.map((doc) => {
      const data = doc.data();
      const createdAt = data.createdAt.toDate();
      const updatedAt = data.updatedAt.toDate();
      return { ...data, createdAt, updatedAt } as StoreDB;
    });
  }

  return [];
}

export { getStore, getStores, getStoreByUserId };
