export const capitalize = (text: string): string => {
  if (typeof text !== "string") {
    return "";
  }
  return text.charAt(0).toUpperCase() + text.slice(1);
};

export const formatByteToKb = (byte: number): string => {
  const sizeInKb = byte / 1024;
  return `${sizeInKb.toFixed(2)}Kb`;
};
