import { Timestamp } from 'firebase/firestore';

interface StoreDB {
  id: string;
  name: string;
  phone?: string;
  address?: string;
  city?: string;
  zipCode?: string;
  latitude?: number;
  longitude?: number;
  userId: string;
  createdAt: Timestamp;
  updatedAt: Timestamp;
}

interface BillboardDB {
  id: string;
  label: string;
  imageUrl: string;
  createdAt: Timestamp;
  updatedAt: Timestamp;
}
interface CategoryDB {
  id: string;
  name: string;
  imageUrl: string;
  description: string;
  createdAt: Timestamp;
  updatedAt: Timestamp;
}

export type { StoreDB, BillboardDB, CategoryDB };
