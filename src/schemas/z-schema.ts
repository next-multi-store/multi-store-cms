import { z } from 'zod';

const phoneIndonesiaRegex = /^(\+62|62|0)8[1-9][0-9]{6,11}$/;

const storeSchema = z.object({
  name: z
    .string({ message: 'stores.name.required' })
    .optional()
    .refine(
      (value) => {
        return value !== undefined && value.trim() !== '';
      },
      {
        message: 'stores.name.required',
      },
    ),
  phone: z
    .string()
    .optional()
    .refine(
      (value) => {
        return value !== undefined && value.trim() !== '';
      },
      {
        message: 'stores.phone.required',
      },
    )
    .refine(
      (value) => {
        return phoneIndonesiaRegex.test(value!);
      },
      {
        message: 'stores.phone.format',
      },
    ),
  address: z
    .string()
    .optional()
    .refine(
      (value) => {
        return value !== undefined && value.trim() !== '';
      },
      {
        message: 'stores.address.required',
      },
    ),
  city: z
    .string()
    .optional()
    .refine(
      (value) => {
        return value !== undefined && value.trim() !== '';
      },
      {
        message: 'stores.city.required',
      },
    ),
  zipCode: z
    .string()
    .optional()
    .refine(
      (value) => {
        return value !== undefined && value.trim() !== '';
      },
      {
        message: 'stores.zipCode.required',
      },
    ),
  latitude: z
    .number()
    .optional()
    .refine(
      (value) => {
        return value !== undefined;
      },
      {
        message: 'stores.latitude.required',
      },
    ),
  longitude: z
    .number()
    .optional()
    .refine(
      (value) => {
        return value !== undefined;
      },
      {
        message: 'stores.longitude.required',
      },
    ),
});

const billboardSchema = z.object({
  label: z
    .string()
    .optional()
    .refine(
      (value) => {
        return value !== undefined && value.trim() !== '';
      },
      {
        message: 'billboards.label.required',
      },
    ),
  imageUrl: z
    .string()
    .optional()
    .refine(
      (value) => {
        return value !== undefined && value.trim() !== '';
      },
      {
        message: 'billboards.image.required',
      },
    ),
});

const categorySchema = z.object({
  name: z
    .string()
    .optional()
    .refine(
      (value) => {
        return value !== undefined && value.trim() !== '';
      },
      {
        message: 'categories.name.required',
      },
    ),
  description: z
    .string()
    .optional()
    .refine(
      (value) => {
        return value !== undefined && value.trim() !== '';
      },
      {
        message: 'categories.description.required',
      },
    ),
  imageUrl: z
    .string()
    .optional()
    .refine(
      (value) => {
        return value !== undefined && value.trim() !== '';
      },
      {
        message: 'categories.image.required',
      },
    ),
});

const productSchema = z.object({
  name: z
    .string()
    .optional()
    .refine(
      (value) => {
        return value !== undefined && value.trim() !== '';
      },
      {
        message: 'products.name.required',
      },
    ),
  description: z
    .string()
    .optional()
    .refine(
      (value) => {
        return value !== undefined && value.trim() !== '';
      },
      {
        message: 'products.description.required',
      },
    ),
  imageUrl: z
    .string()
    .optional()
    .refine(
      (value) => {
        return value !== undefined && value.trim() !== '';
      },
      {
        message: 'products.image.required',
      },
    ),
  categoryId: z
    .string()
    .optional()
    .refine(
      (value) => {
        return value !== undefined && value.trim() !== '';
      },
      {
        message: 'products.category.required',
      },
    ),
  price: z
    .number()
    .optional()
    .refine(
      (value) => {
        return value !== undefined;
      },
      {
        message: 'products.price.required',
      },
    ),
  status: z.boolean().optional(),
});

type Store = z.infer<typeof storeSchema> & { id?: string };
type Billboard = z.infer<typeof billboardSchema> & {
  no?: number;
  id?: string;
  userId?: string;
  createdAt?: string;
  updatedAt?: string;
};
type Category = z.infer<typeof categorySchema> & {
  no?: number;
  id?: string;
  userId?: string;
  createdAt?: string;
  updatedAt?: string;
};

type Product = z.infer<typeof productSchema> & {
  no?: number;
  id?: string;
  userId?: string;
  createdAt?: string;
  updatedAt?: string;
};

export {
  storeSchema,
  billboardSchema,
  categorySchema,
  phoneIndonesiaRegex,
  productSchema,
};
export type { Store, Billboard, Category, Product };
