export type DialogVariant = "add" | "view" | "update" | "delete";

export const DialogVariantValues: Record<DialogVariant, DialogVariant> = {
  add: "add",
  view: "view",
  update: "update",
  delete: "delete",
};
