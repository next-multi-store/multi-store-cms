import { ReactNode } from 'react';
import { Separator } from './ui/separator';

interface HeaderProps {
  title: string;
  description: string;
  actionButton?: ReactNode;
}
const Header = ({ title, description, actionButton }: HeaderProps) => {
  return (
    <>
      <div className="w-full flex md:flex-row flex-col md:justify-between md:items-center gap-4 mb-3">
        <div className="flex flex-col gap-1 grow">
          <h1 className="font-bold text-2xl tracking-tighter">{title}</h1>
          <p className="text-muted-foreground text-sm">{description}</p>
        </div>
        {actionButton && (
          <div className="ml-auto w-full md:w-fit">{actionButton}</div>
        )}
      </div>
      <Separator />
    </>
  );
};

export { Header };
