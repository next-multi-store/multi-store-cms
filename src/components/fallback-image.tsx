import { useState } from 'react';
import Image from 'next/image';

interface FallbackImageProps {
  src: string;
  alt: string;
  width: number;
  height: number;
}

const FallbackImage = ({ src, alt, width, height }: FallbackImageProps) => {
  const [imageSrc, setImageSrc] = useState(src);
  const fallbackSrc = '/images/Image_not_available.png';

  return (
    <Image
      alt={alt}
      height={height}
      src={imageSrc}
      width={width}
      priority
      onError={() => setImageSrc(fallbackSrc)}
    />
  );
};

export { FallbackImage };
