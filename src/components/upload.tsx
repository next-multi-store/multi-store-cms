import { FileSpreadsheet, Plus, UploadIcon, X } from 'lucide-react';
import Image from 'next/image';
import * as React from 'react';

import { buttonVariants } from '@/components/ui/button';
import {
  FormControl,
  FormDescription,
  FormItem,
  FormLabel,
  FormMessage,
  useFormField,
} from '@/components/ui/form';
import { useWindowSize } from '@/hooks/use-window-size';
import { cn } from '@/lib/utils';
import { formatByteToKb } from '@/lib/text';

export interface PreviewProps {
  name: string;
  src: string;
  size?: number;
  isImage: boolean;
}

export interface InputProps
  extends React.InputHTMLAttributes<HTMLInputElement> {
  isResponsive?: boolean;
  label?: string;
  labelPosition?: 'left' | 'top';
  labelWidth?: number;
  description?: string;
  previews: PreviewProps[];
  previewsPosition?: 'left' | 'top';
  previewClassName?: string;
  onRemove: (name: string) => void;
  limit?: number;
}

const Upload = React.forwardRef<HTMLInputElement, InputProps>(
  (
    {
      disabled,
      required,
      isResponsive,
      label,
      labelPosition = 'left',
      labelWidth = 0,
      description,
      placeholder,
      previews,
      previewsPosition = 'left',
      previewClassName,
      onRemove,
      limit = Infinity,
      ...props
    },
    ref,
  ) => {
    const { isMobile } = useWindowSize({ minWidth: 640 });
    const size = isMobile ? 80 : 150;
    const { error: FError } = useFormField();
    const hasError = !!FError;
    const isMaxLimit = previews.length >= limit;

    return (
      <FormItem
        className={cn(
          'flex w-full gap-1.5',
          labelPosition === 'left' ? 'flex-row items-center' : 'flex-col',
          isResponsive && 'flex-col items-stretch sm:items-center sm:flex-row',
        )}
        style={
          {
            '--label-width': `${labelWidth}px`,
            '--size': `${size}px`,
          } as React.CSSProperties
        }
      >
        {label && (
          <FormLabel
            className={cn(
              'shrink-0',
              labelPosition === 'left' && 'sm:basis-[var(--label-width)]',
            )}
          >
            {label}
            {required && <span className="text-destructive">*</span>}
          </FormLabel>
        )}
        <div className="!mt-0 flex flex-1 flex-col">
          <div
            className={cn(
              'flex gap-1 overflow-x-auto pb-1',
              previewsPosition === 'left'
                ? 'flex-row items-center'
                : 'flex-col items-start',
            )}
          >
            {previews.length > 0 &&
              previews.map((preview) => (
                <div
                  key={preview.name}
                  className={cn(
                    preview.isImage
                      ? 'rounded-lg shrink-0 relative border border-stroke'
                      : 'flex justify-between items-center',
                  )}
                >
                  {preview.isImage ? (
                    <Image
                      alt={preview.name}
                      className={cn(
                        'aspect-square rounded-lg object-cover',
                        previewClassName,
                      )}
                      height={size}
                      src={preview.src}
                      width={size}
                    />
                  ) : (
                    <div className="flex items-center gap-2.5">
                      <FileSpreadsheet className="text-primary" size={42} />
                      <div className="flex flex-col">
                        <span className="w-48 truncate text-base font-medium text-primary-dark">
                          {preview.name}
                        </span>
                        {preview.size && (
                          <span className="text-xs font-normal">
                            {formatByteToKb(preview.size)}
                          </span>
                        )}
                      </div>
                    </div>
                  )}
                  <div
                    className={cn(
                      'cursor-pointer',
                      preview.isImage
                        ? 'absolute right-0 top-0 flex size-11 justify-end bg-gradient-45 from-transparent from-50% to-primary-dark to-100% p-1 rounded'
                        : '',
                    )}
                  >
                    <X
                      className={cn(
                        'text-white',
                        disabled && 'hidden',
                        'min-h-4 min-w-4',
                      )}
                      onClick={() => !disabled && onRemove(preview.name)}
                      size={16}
                    />
                  </div>
                </div>
              ))}
            <FormLabel
              className={cn(
                buttonVariants({
                  variant: 'outline',
                  size: 'default',
                }),
                disabled ? 'pointer-events-none opacity-50' : 'cursor-pointer',
                'shrink-0 flex cursor-pointer items-center rounded-md border  p-0 w-[var(--size)] h-[var(--size)]',
                isMaxLimit && 'hidden',
                hasError ? 'border-destructive' : 'border-dashed border-stroke',
              )}
            >
              <UploadIcon size={16} className="min-h-4 min-w-4" />
            </FormLabel>
            <FormControl>
              <input
                ref={ref}
                className="hidden"
                disabled={isMaxLimit || disabled}
                type="file"
                {...props}
              />
            </FormControl>
          </div>
          {description && (
            <FormDescription className="text-sm font-normal text-black">
              {description}
            </FormDescription>
          )}
          <FormMessage className="text-sm font-normal text-destructive" />
        </div>
      </FormItem>
    );
  },
);
Upload.displayName = 'Upload';

export { Upload };
