'use client';

import { useLocale } from 'next-intl';
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from '@/components/ui/select';
import { usePathname, useRouter } from 'next/navigation';
import { useEffect, useState } from 'react';
import { appConfig } from '@/app.config';

const LangSwitcher = () => {
  const router = useRouter();
  const pathname = usePathname();
  const locale = useLocale();
  const [selectedLang, setSelectedLang] = useState<string>(locale);

  useEffect(() => {
    const currentLang = pathname.split('/')[1] ?? '';
    if (!appConfig.i18n.locales.includes(currentLang)) {
      router.push(`/${selectedLang}${pathname}`);
    } else {
      const newPathname = pathname.replace(
        `/${currentLang}`,
        `/${selectedLang}`,
      );
      router.push(newPathname);
    }
    router.refresh();
  }, [selectedLang]);

  return (
    <Select
      onValueChange={(value) => {
        setSelectedLang(value);
      }}
      value={selectedLang}
    >
      <SelectTrigger className="h-8" value={selectedLang}>
        <SelectValue placeholder={'piilih bahasa'} />
      </SelectTrigger>
      <SelectContent side="bottom" className="z-50">
        {appConfig.locales.map((item) => (
          <SelectItem key={item.value} value={item.value}>
            {item.label}
          </SelectItem>
        ))}
      </SelectContent>
    </Select>
  );
};

export { LangSwitcher };
