"use client";

import {
  Dialog,
  DialogHeader,
  DialogContent,
  DialogDescription,
  DialogTitle,
} from "./ui/dialog";

interface ModalProps {
  isOpen: boolean;
  title: string;
  description: string;
  onHandleClose: () => void;
  children?: React.ReactNode;
}

const Modal = ({
  isOpen,
  title,
  description,
  onHandleClose,
  children,
}: ModalProps) => {
  const onChange = (open: boolean) => {
    if (!open) onHandleClose();
  };
  return (
    <Dialog
      open={isOpen}
      onOpenChange={onChange}>
      <DialogContent className='max-h-4/5 w-11/12 overflow-y-auto rounded-lg sm:h-max sm:max-w-lg'>
        <DialogHeader>
          <DialogTitle>{title}</DialogTitle>
          <DialogDescription>{description}</DialogDescription>
        </DialogHeader>
        <div>{children}</div>
      </DialogContent>
    </Dialog>
  );
};

export { Modal };
