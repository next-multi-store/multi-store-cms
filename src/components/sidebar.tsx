import { MenuItem } from './menu-item';

export default function Sidebar() {
  return (
    <div className="md:block hidden w-full md:w-[var(--expandedSidebarWidth)] p-3 border-r  fixed min-h-svh bg-white z-10">
      <MenuItem />
    </div>
  );
}
