'use client';
import { useParams, usePathname, useRouter } from 'next/navigation';
import {
  GalleryThumbnails,
  Gauge,
  Settings,
  LayoutDashboard,
  ChevronDown,
  ChevronUp,
  HardDrive,
  Package,
} from 'lucide-react';
import { cn } from '@/lib/utils';
import { useLocale, useTranslations } from 'next-intl';
import { StoreSwitcher } from './store-switcher';
import { useState, useEffect } from 'react';
import { ScrollArea } from './ui/scroll-area';

interface MenuItemProps {
  onHandleClose?: () => void;
}

interface RouteItem {
  href: string;
  label: string;
  active: boolean;
  icon: JSX.Element;
  subMenu?: RouteItem[];
}

const MenuItem = ({ onHandleClose }: MenuItemProps) => {
  const pathname = usePathname();
  const params = useParams();
  const router = useRouter();
  const t = useTranslations('global');
  const [activeMenu, setActiveMenu] = useState<string[]>([]);
  const locale = useLocale();

  const routes: RouteItem[] = [
    {
      href: `/${locale}/${params.storeId}/dashboards`,
      label: t('routes.dashboard'),
      active: pathname === `/${params.storeId}/dashboards`,
      icon: <Gauge className="size-4 min-h-4 min-w-4" />,
    },
    {
      href: '#',
      label: t('routes.masterData'),
      active: pathname.startsWith(`/${params.storeId}/master-data`),
      icon: <HardDrive className="size-4 min-h-4 min-w-4" />,
      subMenu: [
        {
          href: `/${locale}/${params.storeId}/categories`,
          label: t('routes.categories'),
          active: pathname === `/${params.storeId}/categories`,
          icon: <LayoutDashboard className="size-4 min-h-4 min-w-4" />,
        },
        {
          href: `/${locale}/${params.storeId}/billboards`,
          label: t('routes.billboards'),
          active: pathname === `/${params.storeId}/billboards`,
          icon: <GalleryThumbnails className="size-4 min-h-4 min-w-4" />,
        },
        {
          href: `/${locale}/${params.storeId}/products`,
          label: t('routes.products'),
          active: pathname === `/${params.storeId}/products`,
          icon: <Package className="size-4 min-h-4 min-w-4" />,
        },
      ],
    },
    {
      href: `/${locale}/${params.storeId}/settings`,
      label: t('routes.settings'),
      active: pathname === `/${params.storeId}/settings`,
      icon: <Settings className="size-4 min-h-4 min-w-4" />,
    },
  ];

  // Set default open menus with subMenu
  useEffect(() => {
    const defaultOpenMenus = routes
      .filter((route) => route.subMenu)
      .map((route) => route.label);
    setActiveMenu(defaultOpenMenus);
  }, []);

  const handleLink = (href: string) => {
    if (href === '#') {
      return;
    }
    router.push(href);
    onHandleClose?.();
  };

  const toggleSubMenu = (label: string) => {
    setActiveMenu((prev) =>
      prev.includes(label)
        ? prev.filter((item) => item !== label)
        : [...prev, label],
    );
  };

  return (
    <>
      <div className="mb-3 p-2">
        <StoreSwitcher />
      </div>
      <ScrollArea className="h-full md:h-[calc(100dvh-150px)] w-full p-2 no-scrollbar">
        {routes.map((menu, index) => {
          const hasSubMenu = menu.subMenu && menu.subMenu.length > 0;
          return (
            <div key={index}>
              <div
                onClick={() =>
                  hasSubMenu ? toggleSubMenu(menu.label) : handleLink(menu.href)
                }
                className={cn(
                  'hover:bg-accent flex p-2 flex-row items-center gap-2 text-muted-foreground',
                  menu.active && 'text-black font-semibold',
                )}
              >
                {menu.icon}
                {menu.label}
                {hasSubMenu && (
                  <span className="ml-auto">
                    {activeMenu.includes(menu.label) ? (
                      <ChevronUp />
                    ) : (
                      <ChevronDown />
                    )}
                  </span>
                )}
              </div>
              {hasSubMenu && activeMenu.includes(menu.label) && (
                <div className="ml-4">
                  {menu.subMenu!.map((subMenu, subIndex) => (
                    <div
                      key={subIndex}
                      onClick={() => handleLink(subMenu.href)}
                      className={cn(
                        'hover:bg-accent flex p-2 flex-row items-center gap-2 text-muted-foreground',
                        subMenu.active && 'text-black font-semibold',
                      )}
                    >
                      {subMenu.icon}
                      {subMenu.label}
                    </div>
                  ))}
                </div>
              )}
            </div>
          );
        })}
      </ScrollArea>
    </>
  );
};

export { MenuItem };
