import { UserButton } from '@clerk/nextjs';
import { NavbarItem } from './navbar-item';
import { LangSwitcher } from './lang-switcher';

async function Navbar() {
  return (
    <div className="border-b w-full h-16 flex items-center px-4 ">
      <NavbarItem />
      <div className="ml-auto flex gap-2">
        <LangSwitcher />
        <UserButton afterSignOutUrl="/" />
      </div>
    </div>
  );
}

export { Navbar };
