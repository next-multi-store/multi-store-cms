import { Label } from '@/components/ui/label';
import { cn } from '@/lib/utils';

interface LabelContainerProps {
  children: React.ReactNode;
  containerClassName?: string;
  className?: string;
  id?: string;
  isRequired?: boolean;
  isResponsive?: boolean;
  label?: string;
  labelPosition?: 'left' | 'top';
  labelWidth?: number;
  description?: string;
  error?: string;
  isBgDark?: boolean;
}

export const LabelContainer = ({
  children,
  containerClassName,
  className,
  id,
  isRequired,
  isResponsive = false,
  label,
  labelPosition = 'left',
  labelWidth = 0,
  description,
  error,
  isBgDark = false,
}: LabelContainerProps) => {
  return (
    <div
      className={cn(
        'flex w-full gap-1.5',
        labelPosition === 'left' ? 'flex-row items-center' : 'flex-col',
        isResponsive && 'flex-col items-stretch sm:items-center sm:flex-row',
        containerClassName,
      )}
      style={{ '--label-width': `${labelWidth}px` } as React.CSSProperties}
    >
      {label && (
        <Label
          className={cn(
            'shrink-0',
            labelPosition === 'left' && 'sm:basis-[var(--label-width)]',
            className,
          )}
          htmlFor={id}
        >
          {label}
          {isRequired && <span className="text-destructive">*</span>}
        </Label>
      )}
      <div className="!mt-0 flex flex-1 flex-col">
        {children}
        {description && (
          <p className="text-sm font-normal text-primary-dark">{description}</p>
        )}
        {error && (
          <p
            className={cn(
              'text-sm font-normal',
              isBgDark ? 'text-destructive-foreground' : 'text-destructive',
            )}
          >
            {error}
          </p>
        )}
      </div>
    </div>
  );
};
