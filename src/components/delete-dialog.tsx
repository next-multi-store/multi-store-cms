'use client';

import { useTranslations } from 'next-intl';
import {
  AlertDialog,
  AlertDialogAction,
  AlertDialogCancel,
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogTitle,
} from '@/components/ui/alert-dialog';

interface DeleteDialogProps<T> {
  isOpenDialog: boolean;
  selectedData: T;
  title: string;
  isLoading: boolean;
  onHandleSubmitDelete: (data: T) => void;
  onCloseDialog: () => void;
}

export const DeleteDialog = <T,>({
  isOpenDialog,
  selectedData,
  onHandleSubmitDelete,
  onCloseDialog,
  title,
  isLoading = false,
}: DeleteDialogProps<T>) => {
  const t = useTranslations('global');

  return (
    <AlertDialog
      onOpenChange={(isOpen) => {
        if (!isOpen) onCloseDialog();
      }}
      open={isOpenDialog}
    >
      <AlertDialogContent className="w-11/12 overflow-y-auto rounded-lg sm:h-max sm:max-w-lg">
        <AlertDialogHeader>
          <AlertDialogTitle>{t('warning')}</AlertDialogTitle>
          <AlertDialogDescription>
            {t('dialog.descriptionDelete', { name: title })}
          </AlertDialogDescription>
        </AlertDialogHeader>
        <AlertDialogFooter>
          <AlertDialogCancel onClick={onCloseDialog}>
            {t('cancel')}
          </AlertDialogCancel>
          <AlertDialogAction
            onClick={() => onHandleSubmitDelete(selectedData)}
            isLoading={isLoading}
          >
            {t('delete')}
          </AlertDialogAction>
        </AlertDialogFooter>
      </AlertDialogContent>
    </AlertDialog>
  );
};
