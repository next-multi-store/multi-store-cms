import type { Table } from '@tanstack/react-table';
import { ArrowLeft, ArrowRight, ChevronLeft, ChevronRight } from 'lucide-react';

import { Button } from '@/components/ui/button';
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from '@/components/ui/select';
import { useTranslations } from 'next-intl';

interface DataTablePaginationProps<TData> {
  table: Table<TData>;
}

export const DataTablePagination = <TData,>({
  table,
}: DataTablePaginationProps<TData>) => {
  const t = useTranslations('global');
  return (
    <div className="flex items-center justify-end p-3 gap-4">
      <div className="flex items-center">
        <Select
          onValueChange={(value) => {
            table.setPageSize(Number(value));
          }}
          value={`${table.getState().pagination.pageSize}`}
        >
          <SelectTrigger
            className="h-8"
            value={table.getState().pagination.pageSize}
          >
            <SelectValue placeholder={table.getState().pagination.pageSize} />
          </SelectTrigger>
          <SelectContent side="top">
            {[1, 5, 10, 20, 30, 40, 50].map((pageSize) => (
              <SelectItem key={pageSize} value={`${pageSize}`}>
                {pageSize}/{t('page').toLowerCase()}
              </SelectItem>
            ))}
          </SelectContent>
        </Select>
      </div>
      <div className="flex  items-center justify-center text-sm font-medium">
        {t('page')} {table.getState().pagination.pageIndex + 1} {t('of')}{' '}
        {table.getPageCount()}
      </div>
      <div className="flex items-center gap-2">
        <Button
          className="hidden size-8 p-0 lg:flex"
          disabled={!table.getCanPreviousPage()}
          onClick={() => table.setPageIndex(0)}
          variant="outline"
        >
          <span className="sr-only">{t('GoToFirstPage')}</span>
          <ArrowLeft className="size-4" />
        </Button>
        <Button
          className="size-8 p-0"
          disabled={!table.getCanPreviousPage()}
          onClick={() => table.previousPage()}
          variant="outline"
        >
          <span className="sr-only">{t('GoToPreviousPage')}</span>
          <ChevronLeft className="size-4" />
        </Button>
        <Button
          className="size-8 p-0"
          disabled={!table.getCanNextPage()}
          onClick={() => table.nextPage()}
          variant="outline"
        >
          <span className="sr-only">{t('GoToNextPage')}</span>
          <ChevronRight className="size-4" />
        </Button>
        <Button
          className="hidden size-8 p-0 lg:flex"
          disabled={!table.getCanNextPage()}
          onClick={() => table.setPageIndex(table.getPageCount())}
          variant="outline"
        >
          <span className="sr-only">{t('GoToLastPage')}</span>
          <ArrowRight className="size-4" />
        </Button>
      </div>
    </div>
  );
};
