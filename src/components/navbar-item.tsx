'use client';
import { Drawer, DrawerContent, DrawerHeader, DrawerTitle } from './ui/drawer';
import { Button } from './ui/button';
import { AlignLeft, X } from 'lucide-react';
import { MenuItem } from './menu-item';
import { useState } from 'react';
import { useTranslations } from 'next-intl';

function NavbarItem() {
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const t = useTranslations('global');
  const handleDrawer = () => {
    setIsOpen((oldVal) => !oldVal);
  };

  return (
    <>
      <div className="flex flex-row-reverse md:flex-row">
        <Button
          type="button"
          onClick={handleDrawer}
          className="md:hidden block"
          size={'icon'}
          variant={'ghost'}
        >
          <AlignLeft />
        </Button>
      </div>
      <Drawer direction="left" open={isOpen}>
        <DrawerContent>
          <DrawerHeader>
            <DrawerTitle className="flex flex-row">
              {t('menu')} <X className="ml-auto" onClick={handleDrawer} />
            </DrawerTitle>
          </DrawerHeader>
          <MenuItem onHandleClose={handleDrawer} />
        </DrawerContent>
      </Drawer>
    </>
  );
}

export { NavbarItem };
