import {
  GoogleMap,
  type GoogleMapProps,
  useJsApiLoader,
  Marker,
  Autocomplete,
  Libraries,
} from '@react-google-maps/api';
import { memo, useEffect, useCallback, useRef, useState } from 'react';

import { LabelContainer } from '@/components/label-container';
import {
  FormControl,
  FormDescription,
  FormItem,
  FormLabel,
  FormMessage,
} from '@/components/ui/form';
import { env } from '@/env.mjs';
import { cn } from '@/lib/utils';
import { useTranslations } from 'next-intl';

interface GmapsProps extends GoogleMapProps {
  className?: string;
  hasForm?: boolean;
  isRequired?: boolean;
  isResponsive?: boolean;
  label?: string;
  labelPosition?: 'left' | 'top';
  labelWidth?: number;
  description?: string;
  error?: string;
  onHandlePinMap: (event: google.maps.MapMouseEvent) => void;
  marker: { lat: number; lng: number } | null;
}

const center = {
  lat: -6.176166,
  lng: 106.826247,
};
const libraries: Libraries = ['places'];

const GmapsCont = ({
  children,
  className,
  hasForm = false,
  isRequired,
  isResponsive,
  label,
  labelPosition = 'left',
  labelWidth = 0,
  description,
  error,
  onHandlePinMap,
  marker,
  ...props
}: GmapsProps) => {
  const t = useTranslations();
  const { isLoaded } = useJsApiLoader({
    id: 'google-map-script',
    googleMapsApiKey: env.NEXT_PUBLIC_GOOGLE_MAPS_API_KEY,
    libraries,
  });
  const autocompleteRef = useRef<google.maps.places.Autocomplete | null>(null);
  const [selectedPlace, setSelectedPlace] = useState<{
    lat: number;
    lng: number;
  } | null>(null);

  const defaultOptions: google.maps.MapOptions = {
    zoomControl: true,
    mapTypeControl: false,
    scaleControl: false,
    streetViewControl: false,
    rotateControl: false,
    fullscreenControl: true,
    disableDefaultUI: false,
    disableDoubleClickZoom: true,
  };

  const handlePlaceSelect = useCallback(() => {
    if (autocompleteRef.current !== null) {
      const place = autocompleteRef.current.getPlace();
      if (place && place.geometry && place.geometry.location) {
        setSelectedPlace({
          lat: place.geometry.location.lat(),
          lng: place.geometry.location.lng(),
        });
      }
    }
  }, []);

  useEffect(() => {
    if (marker) {
      setSelectedPlace({
        lat: marker.lat,
        lng: marker.lng,
      });
    }
  }, [marker]);
  if (!isLoaded) return null;

  const GmapsComp = (
    <GoogleMap
      mapContainerClassName={cn('w-full h-56 rounded-lg', className)}
      zoom={15}
      center={selectedPlace || center}
      onClick={onHandlePinMap}
      options={defaultOptions}
      {...props}
    >
      <Autocomplete
        onLoad={(autocomplete) => (autocompleteRef.current = autocomplete)}
        onPlaceChanged={handlePlaceSelect}
      >
        <input
          autoFocus={false}
          autoComplete={'off'}
          type="text"
          placeholder={t('stores.searchAddress')}
          className="rounded-full border-transparent w-72 h-8 py-3 px-4 shadow-sm text-sm outline-none absolute left-1/2 -translate-x-1/2 top-[5%]"
          onKeyDown={(event) => {
            if (event.key === 'Enter') {
              event.preventDefault();
            }
          }}
        />
      </Autocomplete>
      {marker && (
        <Marker key={1} position={{ lat: marker.lat, lng: marker.lng }} />
      )}
      {children}
    </GoogleMap>
  );

  if (!hasForm)
    return (
      <LabelContainer
        description={description}
        error={error}
        isRequired={isRequired}
        isResponsive={isResponsive}
        label={label}
        labelPosition={labelPosition}
        labelWidth={labelWidth}
      >
        {GmapsComp}
      </LabelContainer>
    );

  return (
    <FormItem
      className={cn(
        'flex w-full gap-1.5',
        labelPosition === 'left' ? 'flex-row items-center' : 'flex-col',
        isResponsive && 'flex-col items-stretch sm:items-center sm:flex-row',
      )}
      style={{ '--label-width': `${labelWidth}px` } as React.CSSProperties}
    >
      {label && (
        <FormLabel
          className={cn(
            'shrink-0',
            labelPosition === 'left' && 'sm:basis-[var(--label-width)]',
          )}
        >
          {label}
          {isRequired && <span className="text-destructive">*</span>}
        </FormLabel>
      )}
      <div className="!mt-0 flex flex-1 flex-col">
        <FormControl>{GmapsComp}</FormControl>
        {description && (
          <FormDescription className="text-sm font-normal text-primary-dark">
            {description}
          </FormDescription>
        )}
        <FormMessage className="text-sm font-normal text-error" />
      </div>
    </FormItem>
  );
};

const Gmaps = memo(GmapsCont);

export { Gmaps };
