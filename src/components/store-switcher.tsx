'use client';

import { Popover, PopoverContent, PopoverTrigger } from './ui/popover';
import { StoreDB } from '@/schemas/db';
import { useRouter, useParams } from 'next/navigation';
import { ChangeEvent, useCallback, useEffect, useState } from 'react';
import { Button } from './ui/button';
import { Check, ChevronsUpDown, PlusCircle, StoreIcon } from 'lucide-react';
import {
  Command,
  CommandEmpty,
  CommandGroup,
  CommandSeparator,
  CommandList,
} from './ui/command';

import { Input } from './ui/input';
import { cn } from '@/lib/utils';
import { useStoreModal } from '@/hooks/use-store-modal';
import { useMounted } from '@/hooks/use-mounted';
import { useTranslations } from 'next-intl';
import { useAuth } from '@clerk/nextjs';
import { getStores } from '@/lib/get-store';

type StoreType = {
  label: string;
  value: string;
};

const StoreSwitcher = () => {
  const { storeId } = useParams();
  const router = useRouter();
  const [open, setOpen] = useState<boolean>(false);
  const [search, setSearch] = useState<string>('');
  const [filtered, setFiltered] = useState<StoreType[]>([]);
  const handleClose = useStoreModal((state) => state.onHandleClose);
  const isOpen = useStoreModal((state) => state.isOpen);
  const { isMounted } = useMounted();
  const t = useTranslations();
  const { userId } = useAuth();
  const [stores, setStores] = useState<StoreDB[]>();

  const fetchStoreData = useCallback(async () => {
    if (!userId) return;

    try {
      const data = await getStores(userId);
      if (data) {
        setStores(data);
      }
    } catch (error) {
      console.error('Failed to fetch store:', error);
    }
  }, [userId]);

  useEffect(() => {
    fetchStoreData();
  }, []);

  useEffect(() => {
    if (isOpen && !isMounted) handleClose();
  }, [handleClose, isOpen, isMounted, storeId]);

  const formattedStores =
    stores?.map((store) => {
      return { label: store.name, value: store.id };
    }) ?? [];

  const storeItems = search === '' ? formattedStores : filtered;
  const selectedStore = formattedStores.find((item) => item.value === storeId);

  const handleSelectedStore = (store: StoreType) => {
    setOpen(false);
    router.push(`/${store.value}/dashboards`);
  };

  const handleSearch = (e: ChangeEvent<HTMLInputElement>) => {
    setSearch(e.target.value);
    setFiltered(
      formattedStores.filter((item) =>
        item.label.toLocaleLowerCase().includes(search.toLowerCase()),
      ),
    );
  };

  const handleCreateStore = useStoreModal((state) => state.onHandleOpen);

  return (
    <Popover open={open} onOpenChange={setOpen}>
      <PopoverTrigger asChild>
        <Button
          variant="outline"
          role="combobox"
          aria-expanded={open}
          className="w-full justify-between"
        >
          <div className="w-full  flex gap-3 items-center justify-start">
            <StoreIcon className="h-4 w-4 min-h-4 min-w-4" />
            {selectedStore?.value
              ? formattedStores.find(
                  (store) => store.value === selectedStore?.value,
                )?.label
              : t('stores.form.label.optionStore')}
          </div>

          <ChevronsUpDown className="ml-2 h-4 w-4 shrink-0 opacity-50" />
        </Button>
      </PopoverTrigger>
      <PopoverContent className="w-[200px] p-0">
        <Command>
          <div className="w-full px-2 py-3">
            <Input
              type="text"
              placeholder={t('stores.search')}
              onChange={handleSearch}
              className="flex-1 w-full outline-none !px-2 !py-1"
            />
          </div>
          <CommandList>
            <CommandGroup heading={t('stores.form.label.name')}>
              {storeItems.length > 0 ? (
                storeItems.map((item) => {
                  const isChecked = selectedStore?.value === item.value;
                  return (
                    <div
                      key={item.value}
                      className="flex items-center px-2 py-1 cursor-pointer hover:bg-gray-50 text-slate-700 hover:text-primary  transition-colors"
                      onClick={() => handleSelectedStore(item)}
                    >
                      <p className="w-full truncate text-sm whitespace-nowrap">
                        {item.label}
                      </p>
                      <Check
                        className={cn(
                          'ml-auto w-4 h-4 min-h-4 min-w-4',
                          isChecked ? 'opacity-100' : 'opacity-0',
                        )}
                      />
                    </div>
                  );
                })
              ) : (
                <CommandEmpty className="p-2 text-sm">
                  {t('stores.empty')}
                </CommandEmpty>
              )}
            </CommandGroup>
          </CommandList>
          <CommandSeparator />
          <CommandList>
            <CommandGroup>
              <div
                className="flex items-center hover:bg-gray-50 px-2 py-1 cursor-pointer text-sm text-muted-foreground hover:text-primary gap-2"
                onClick={() => handleCreateStore()}
              >
                <PlusCircle className="h-5 w-5 min-h-5 min-w-5" />
                {t('global.addNew')}
              </div>
            </CommandGroup>
          </CommandList>
        </Command>
      </PopoverContent>
    </Popover>
  );
};

export { StoreSwitcher, type StoreType };
