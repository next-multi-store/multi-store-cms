'use client';

import { useState } from 'react';
import { Modal } from '@/components/modal';
import { useStoreModal } from '@/hooks/use-store-modal';
import { useForm } from 'react-hook-form';
import { Store, storeSchema } from '@/schemas/z-schema';
import { zodResolver } from '@hookform/resolvers/zod';
import { Form, FormField } from '@/components/ui/form';
import { Input } from '@/components//ui/input';
import { Button } from './ui/button';
import axios from 'axios';
import { useToast } from '@/hooks/use-toast';
import { useMounted } from '@/hooks/use-mounted';
import { useTranslations } from 'next-intl';

const StoreModal = () => {
  const handleClose = useStoreModal((state) => state.onHandleClose);
  const isOpen = useStoreModal((state) => state.isOpen);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const { toast } = useToast();
  const t = useTranslations();
  const { isMounted } = useMounted();
  const form = useForm<Store>({
    resolver: zodResolver(storeSchema),
    defaultValues: { name: '' },
  });

  const handleSubmit = async (values: Store) => {
    try {
      setIsLoading(true);
      const { status, data } = await axios.post('/api/stores', values);

      if (status !== 201)
        throw new Error('Failed to submit data. Please try again.');

      toast({
        title: t('global.success'),
        description: t('global.dialog.success.insertNew', {
          name: values.name,
        }),
        variant: 'success',
        duration: 1000,
        redirectUrl: `/${data.id}/dashboards`,
      });
    } catch (error) {
      toast({
        title: t('global.error'),
        description: t('global.somethingWrong'),
        variant: 'error',
      });
    } finally {
      setIsLoading(false);
      form.reset();
    }
  };

  if (!isMounted) return null;

  return (
    <Modal
      title={t('global.dialog.titleNew', { name: t('stores.title') })}
      description={t('stores.description')}
      isOpen={isOpen}
      onHandleClose={handleClose}
    >
      <Form {...form}>
        <form onSubmit={form.handleSubmit(handleSubmit)}>
          <FormField
            control={form.control}
            name="name"
            render={({ field }) => {
              return (
                <Input
                  disabled={isLoading}
                  hasForm
                  isResponsive
                  label={t('stores.form.label.name')}
                  labelWidth={100}
                  placeholder={t('stores.form.placeholder.name')}
                  required
                  {...field}
                />
              );
            }}
          />

          <div className="pt-6 space-x-2 flex items-center justify-end w-full">
            <Button
              type="button"
              variant={'outline'}
              onClick={handleClose}
              size="sm"
            >
              {t('global.cancel')}
            </Button>
            <Button isLoading={isLoading} type="submit" size="sm">
              {t('global.continue')}
            </Button>
          </div>
        </form>
      </Form>
    </Modal>
  );
};

export { StoreModal };
