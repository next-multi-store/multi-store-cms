import { ChevronDown, Eye, FilePenLine, Trash2 } from 'lucide-react';

import { Button } from '@/components/ui/button';
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuTrigger,
} from '@/components/ui/dropdown-menu';
import { useTranslations } from 'next-intl';

export type Item = Record<
  string,
  string | number | boolean | string[] | object
>;

export interface Handle {
  name: 'view' | 'update' | 'delete';
  callback: (item: Item) => void;
}

interface TableRowActionProps {
  row: Item;
  handles: Handle[];
}

const DataTableRowAction = ({ row, handles }: TableRowActionProps) => {
  const t = useTranslations('global');
  const action = {
    view: {
      label: t('view'),
      icon: <Eye size={16} />,
    },
    update: {
      label: t('update'),
      icon: <FilePenLine size={16} />,
    },
    delete: {
      label: t('delete'),
      icon: <Trash2 size={16} />,
    },
  };

  return (
    <DropdownMenu>
      <DropdownMenuTrigger asChild>
        <Button
          className="flex items-center gap-2"
          variant="outline"
          size={'sm'}
        >
          <span className="sr-only">{t('openMenu')}</span>
          <span>{t('action')}</span>
          <ChevronDown className="size-4" />
        </Button>
      </DropdownMenuTrigger>
      <DropdownMenuContent align="end">
        {handles.map(({ name, callback }: Handle) => {
          return (
            <DropdownMenuItem
              key={name}
              className="flex items-center gap-2"
              onClick={() => callback(row)}
            >
              {action[name].icon}
              {action[name].label}
            </DropdownMenuItem>
          );
        })}
      </DropdownMenuContent>
    </DropdownMenu>
  );
};

export { DataTableRowAction };
