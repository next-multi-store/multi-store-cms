'use client';

import { Copy } from 'lucide-react';
import { Button } from './ui/button';
import {
  Tooltip,
  TooltipContent,
  TooltipProvider,
  TooltipTrigger,
} from './ui/tooltip';
import { toast } from '@/hooks/use-toast';
import { useTranslations } from 'next-intl';

interface ButtonCopyProps {
  description: string;
}
const ButtonCopy = ({ description }: ButtonCopyProps) => {
  const t = useTranslations('global');
  const handleCopy = () => {
    navigator.clipboard.writeText(description);
    toast({
      title: 'Success',
      description: `URL has been copied`,
      variant: 'success',
    });
  };

  return (
    <TooltipProvider>
      <Tooltip>
        <TooltipTrigger asChild>
          <Button
            onClick={handleCopy}
            variant={'outline'}
            size={'icon'}
            className="w-full md:w-9"
          >
            <Copy className="size-4 min-h-4 min-w-4 " />
          </Button>
        </TooltipTrigger>
        <TooltipContent>
          <p>{t('copyUrl')}</p>
        </TooltipContent>
      </Tooltip>
    </TooltipProvider>
  );
};

export { ButtonCopy };
