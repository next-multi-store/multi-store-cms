'use client';

import * as SwitchPrimitives from '@radix-ui/react-switch';
import * as React from 'react';

import { LabelContainer } from '@/components/label-container';
import {
  FormControl,
  FormDescription,
  FormItem,
  FormLabel,
  FormMessage,
} from '@/components/ui/form';
import { Label } from '@/components/ui/label';
import { cn } from '@/lib/utils';

export interface SwitchProps
  extends React.ComponentPropsWithoutRef<typeof SwitchPrimitives.Root> {
  hasForm?: boolean;
  isResponsive?: boolean;
  label?: string;
  labelPosition?: 'left' | 'top';
  labelWidth?: number;
  labelActive?: string;
  labelInactive?: string;
  description?: string;
  error?: string;
}

const Switch = React.forwardRef<
  React.ElementRef<typeof SwitchPrimitives.Root>,
  SwitchProps
>(
  (
    {
      checked,
      className,
      hasForm = false,
      isResponsive,
      label,
      labelPosition = 'left',
      labelWidth = 0,
      labelActive,
      labelInactive,
      description,
      error,
      ...props
    },
    ref,
  ) => {
    const mainLabelCn = 'text-sm font-medium leading-5.5 text-primary-dark';

    const SwitchComp = (
      <SwitchPrimitives.Root
        ref={ref}
        checked={checked}
        className={cn(
          'peer inline-flex h-7 w-11 shrink-0 cursor-pointer items-center rounded-full border-2 border-transparent transition-colors focus-visible:outline-none focus-visible:ring-1 focus-visible:ring-primary-dark focus-visible:ring-offset-0 disabled:cursor-not-allowed disabled:opacity-50 data-[state=checked]:bg-success data-[state=unchecked]:bg-slate-200',
          className,
        )}
        {...props}
      >
        <SwitchPrimitives.Thumb
          className={cn(
            'pointer-events-none block h-6 w-6 rounded-full bg-white shadow-lg ring-0 transition-transform data-[state=checked]:translate-x-4 data-[state=unchecked]:translate-x-0',
          )}
        />
      </SwitchPrimitives.Root>
    );

    if (!hasForm)
      return (
        <LabelContainer
          description={description}
          error={error}
          isResponsive={isResponsive}
          label={label}
          labelPosition={labelPosition}
          labelWidth={labelWidth}
        >
          <div className="flex items-center gap-2">
            {SwitchComp}
            {labelActive && labelInactive && (
              <Label className={mainLabelCn}>
                {checked ? labelActive : labelInactive}
              </Label>
            )}
          </div>
        </LabelContainer>
      );

    return (
      <FormItem
        className={cn(
          'flex w-full gap-1.5',
          labelPosition === 'left' ? 'flex-row items-center' : 'flex-col',
          isResponsive && 'flex-col items-stretch sm:items-center sm:flex-row',
        )}
        style={{ '--label-width': `${labelWidth}px` } as React.CSSProperties}
      >
        {label && (
          <FormLabel
            className={cn(
              'shrink-0',
              labelPosition === 'left' && 'sm:basis-[var(--label-width)]',
            )}
          >
            {label}
          </FormLabel>
        )}
        <div className="!mt-0 flex flex-1 flex-col">
          <div className="flex items-center gap-2">
            <FormControl>{SwitchComp}</FormControl>
            {labelActive && labelInactive && (
              <FormLabel className={mainLabelCn}>
                {checked ? labelActive : labelInactive}
              </FormLabel>
            )}
          </div>
          {description && (
            <FormDescription className="text-sm font-normal text-primary-dark">
              {description}
            </FormDescription>
          )}
          <FormMessage className="text-sm font-normal text-error" />
        </div>
      </FormItem>
    );
  },
);
Switch.displayName = SwitchPrimitives.Root.displayName;

export { Switch };
