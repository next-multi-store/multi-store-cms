import * as React from 'react';

import { LabelContainer } from '@/components/label-container';
import {
  FormControl,
  FormDescription,
  FormItem,
  FormLabel,
  FormMessage,
  useFormField,
} from '@/components/ui/form';
import { cn } from '@/lib/utils';

export interface InputProps
  extends React.InputHTMLAttributes<HTMLInputElement> {
  hasForm?: boolean;
  isResponsive?: boolean;
  label?: string;
  labelPosition?: 'left' | 'top';
  labelWidth?: number;
  labelClassName?: string;
  description?: string;
  error?: string;
  pre?: React.ReactNode | string;
  suf?: React.ReactNode | string;
  preClassName?: string;
  sufClassName?: string;
  containerClassName?: string;
  isBgDark?: boolean;
}

const Input = React.forwardRef<HTMLInputElement, InputProps>(
  (
    {
      type = 'text',
      disabled,
      required,
      hasForm = false,
      isResponsive,
      label,
      labelPosition = 'left',
      labelWidth = 0,
      labelClassName,
      description,
      error,
      pre,
      suf,
      preClassName,
      sufClassName,
      containerClassName,
      className,
      isBgDark = false,
      ...props
    },
    ref,
  ) => {
    let hasError = false;
    if (hasForm) {
      const { error: FError } = useFormField();
      hasError = !!FError;
    }

    const PreInputComp = (
      <span
        className={cn(
          'rounded-l-lg border-r border-input bg-slate-200 px-3 py-2 text-sm font-bold',
          disabled ? 'bg-disabled text-secondary-light' : '',
          preClassName,
        )}
      >
        {pre}
      </span>
    );

    const SufInputComp = (
      <span
        className={cn(
          'rounded-l-lg border-r border-input bg-prefix px-3 py-2text-sm font-bold',
          disabled ? 'bg-disabled text-secondary-light' : '',
          sufClassName,
        )}
      >
        {suf}
      </span>
    );

    const InputComp = (
      <input
        ref={ref}
        className={cn(
          'flex w-full bg-background px-3 py-2 text-sm text-primary-ring-offset-background file:border-0 file:bg-transparent file:text-sm file:font-medium placeholder:text-muted-foreground disabled:cursor-not-allowed disabled:bg-disabled disabled:text-secondary-light focus-visible:outline-none focus-visible:ring-offset-0 leading-5',
          Boolean(!pre && !suf) && 'rounded-lg',
          Boolean(pre && !suf) && 'rounded-r-lg',
          Boolean(!pre && suf) && 'rounded-l-lg',
          className,
        )}
        disabled={disabled}
        type={type}
        {...props}
      />
    );

    if (!hasForm)
      return (
        <LabelContainer
          className={labelClassName}
          description={description}
          error={error}
          isBgDark={isBgDark}
          isRequired={required}
          isResponsive={isResponsive}
          label={label}
          labelPosition={labelPosition}
          labelWidth={labelWidth}
        >
          <div
            className={cn(
              'flex flex-1 items-center rounded-lg border border-stroke focus-within:ring-1 focus-within:ring-primary-dark',
              disabled ? 'bg-disabled cursor-not-allowed' : '',
              containerClassName,
            )}
          >
            {pre && PreInputComp}
            {InputComp}
            {suf && SufInputComp}
          </div>
        </LabelContainer>
      );

    return (
      <FormItem
        className={cn(
          'flex w-full gap-1.5',
          labelPosition === 'left' ? 'flex-row items-center' : 'flex-col',
          isResponsive && 'flex-col items-stretch sm:items-center sm:flex-row',
        )}
        style={{ '--label-width': `${labelWidth}px` } as React.CSSProperties}
      >
        {label && (
          <FormLabel
            className={cn(
              'shrink-0',
              labelPosition === 'left' && 'sm:basis-[var(--label-width)]',
              labelClassName,
            )}
          >
            {label}
            {required && <span className="text-destructive">*</span>}
          </FormLabel>
        )}
        <div className="!mt-0 flex flex-1 flex-col">
          <div
            className={cn(
              'flex flex-1 items-center rounded-lg border',
              disabled ? 'bg-disabled cursor-not-allowed' : '',
              hasError
                ? 'border-destructive'
                : 'border-stroke focus-within:ring-1 focus-within:ring-black',
              containerClassName,
            )}
          >
            {pre && PreInputComp}
            <FormControl>{InputComp}</FormControl>
            {suf && SufInputComp}
          </div>
          {description && (
            <FormDescription className="text-sm font-normal text-primary-dark">
              {description}
            </FormDescription>
          )}
          <FormMessage
            className={cn('text-sm font-normal', 'text-destructive')}
          />
        </div>
      </FormItem>
    );
  },
);
Input.displayName = 'Input';

export { Input };
