import * as React from 'react';

import { LabelContainer } from '@/components/label-container';
import {
  FormControl,
  FormDescription,
  FormItem,
  FormLabel,
  FormMessage,
  useFormField,
} from './form';
import { cn } from '@/lib/utils';

export interface TextareaProps
  extends React.TextareaHTMLAttributes<HTMLTextAreaElement> {
  hasForm?: boolean;
  isResponsive?: boolean;
  label?: string;
  labelPosition?: 'left' | 'top';
  labelWidth?: number;
  description?: string;
  error?: string;
}

const Textarea = React.forwardRef<HTMLTextAreaElement, TextareaProps>(
  (
    {
      required,
      className,
      hasForm = false,
      isResponsive,
      label,
      labelPosition = 'left',
      labelWidth = 0,
      description,
      error,
      ...props
    },
    ref,
  ) => {
    const { error: FError } = useFormField();
    const hasError = !!FError;

    const TextareaComp = (
      <textarea
        ref={ref}
        className={cn(
          'flex min-h-[80px] resize-none w-full rounded-md bg-background border px-3 py-2 text-sm ring-offset-background placeholder:text-secondary-light focus-visible:outline-none disabled:cursor-not-allowed disabled:bg-disabled disabled:text-secondary-light placeholder:text-muted-foreground',
          className,
          hasError
            ? 'border-destructive'
            : 'border-stroke focus-visible:ring-1 focus-visible:ring-black focus-visible:ring-offset-0',
        )}
        {...props}
      />
    );

    if (!hasForm)
      return (
        <LabelContainer
          description={description}
          error={error}
          isRequired={required}
          isResponsive={isResponsive}
          label={label}
          labelPosition={labelPosition}
          labelWidth={labelWidth}
        >
          {TextareaComp}
        </LabelContainer>
      );

    return (
      <FormItem
        className={cn(
          'flex w-full gap-1.5',
          labelPosition === 'left' ? 'flex-row items-center' : 'flex-col',
          isResponsive && 'flex-col items-stretch sm:items-center sm:flex-row',
        )}
        style={{ '--label-width': `${labelWidth}px` } as React.CSSProperties}
      >
        {label && (
          <FormLabel
            className={cn(
              'shrink-0',
              labelPosition === 'left' && 'sm:basis-[var(--label-width)]',
            )}
          >
            {label}
            {required && <span className="text-destructive">*</span>}
          </FormLabel>
        )}
        <div className="!mt-0 flex flex-1 flex-col gap-1">
          <FormControl>{TextareaComp}</FormControl>
          {description && (
            <FormDescription className="text-sm font-normal text-black">
              {description}
            </FormDescription>
          )}
          <FormMessage className="text-sm font-normal text-destructive" />
        </div>
      </FormItem>
    );
  },
);
Textarea.displayName = 'Textarea';

export { Textarea };
