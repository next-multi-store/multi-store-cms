/* eslint-disable consistent-return */
/* eslint-disable react-hooks/exhaustive-deps */
import type React from 'react';
import { useEffect, useRef, useState } from 'react';

import type { PreviewProps } from '@/components/upload';
import { useToast } from '@/hooks/use-toast';
import {
  createFileSrcsByPreviews,
  createFileUrlsByPreviews,
  createPreviewsByFileUrls,
  validateSize,
  validateType,
} from '@/lib/file';
import { uploadFilesToFirebase } from '@/lib/firebase';

import { useTranslations } from 'next-intl';

export interface UploadOption {
  maxSize?: number;
  accepts?: string[];
  initialFileUrls?: string[];
}

export interface HandleFileOption {
  callback: (selected: string | string[]) => void;
  isSingleFile?: boolean;
}

export const useUpload = (option?: UploadOption) => {
  const fileInputRef = useRef<HTMLInputElement>(null);
  const accepts = option?.accepts ?? [];
  const maxSize = option?.maxSize || 512;
  const initialFileUrls = option?.initialFileUrls ?? [];
  const t = useTranslations();
  const { toast } = useToast();

  const [files, setFiles] = useState<File[]>([]);
  const [previews, setPreviews] = useState<PreviewProps[]>(() =>
    createPreviewsByFileUrls(initialFileUrls),
  );
  const [fileUrls, setFileUrls] = useState<string[]>(initialFileUrls);

  useEffect(() => {
    setFileUrls(() => {
      // Filter previews to get its src url
      const filteredFileUrls = createFileUrlsByPreviews(previews);

      return filteredFileUrls;
    });
  }, [previews]);

  const handleSelect = async (
    event: React.ChangeEvent<HTMLInputElement>,
    { callback, isSingleFile = false }: HandleFileOption,
  ) => {
    const file: File | undefined = event.target.files?.[0];

    // Return if no file or the same file is already exist
    if (!file) return;
    if (files.find((prevFile) => prevFile.name === file.name)) return;

    const { isValidType, isImage } = validateType(file, { accepts });
    const { isValidSize } = validateSize(file, { maxSize });

    if (!isValidType) {
      toast({
        title: t('global.warning'),
        description: t('global.invalidFile'),
        variant: 'warning',
      });
      return;
    }

    if (!isValidSize) {
      toast({
        title: t('global.warning'),
        description: t('global.invalidFileSize', {
          maxSize: maxSize,
        }),
        variant: 'warning',
      });
      return;
    }

    const reader = new FileReader();
    reader.onload = () => {
      const preview = reader.result;
      if (typeof preview !== 'string') return;

      const newPreview = {
        name: file.name,
        src: preview,
        size: file.size,
        isImage,
      };

      setFiles((prevFiles) => [...prevFiles, file]);
      setPreviews((prevPreviews) => [...prevPreviews, newPreview]);

      const newPreviews = [...previews, newPreview];
      const newFileUrls = createFileSrcsByPreviews(newPreviews);
      callback(isSingleFile ? newFileUrls[0] ?? '' : newFileUrls);
    };

    reader.readAsDataURL(file);
  };

  const handleRemove = (
    name: string,
    { callback, isSingleFile = false }: HandleFileOption,
  ) => {
    const condition = (file: File | PreviewProps) => file.name !== name;

    setFiles((prevFiles) => prevFiles.filter(condition));
    setPreviews((prevPreviews) => prevPreviews.filter(condition));

    const filteredPreviews = previews.filter(condition);
    const filteredFileUrls = createFileSrcsByPreviews(filteredPreviews);
    callback(isSingleFile ? filteredFileUrls[0] ?? '' : filteredFileUrls);
    if (fileInputRef.current) {
      fileInputRef.current.value = '';
    }
  };

  const upload = async (src: string) => {
    if (files.length === 0) return fileUrls;

    try {
      const nextFileUrls = await uploadFilesToFirebase(files, src);
      setFiles([]); // Clear the files after upload
      setFileUrls((prevFileUrls) => [...prevFileUrls, ...nextFileUrls]);
      return nextFileUrls;
    } catch (error) {
      console.error('Error uploading files:', error);
      return [];
    }
  };

  const handleResetUpload = () => {
    // Reset value input file
    if (fileInputRef.current) {
      fileInputRef.current.value = '';
    }
    setFiles([]);
    setPreviews([]);
    setFileUrls([]);
  };

  const setPreviewsByFileUrls = (urlFiles: string[]) => {
    setPreviews(() => createPreviewsByFileUrls(urlFiles));
  };

  return {
    previews,
    setPreviews,
    handleSelect,
    handleRemove,
    upload,
    handleResetUpload,
    fileInputRef,
    setPreviewsByFileUrls,
  };
};
