import { useMounted } from "./use-mounted";

const useOriginUrl = (): string | null => {
  const mounted = useMounted;
  const origin =
    typeof window !== "undefined" && window.location.origin
      ? window.location.origin
      : "";

  if (!mounted) return null;
  return origin;
};

export { useOriginUrl };
