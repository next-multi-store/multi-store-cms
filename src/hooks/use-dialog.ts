import { useState } from "react";
import { DialogVariant } from "@/schemas/dialog";

const useDialog = () => {
  const [selectedDialog, setSelectedDialog] = useState<string | undefined>();

  const handleShowDialog = (type: DialogVariant | undefined) => {
    setSelectedDialog(type);
  };

  return { selectedDialog, handleShowDialog };
};

const useDialogWithActions = <T>() => {
  const { selectedDialog, handleShowDialog } = useDialog();
  const [selectedData, setSelectedData] = useState<T | undefined>();

  const handleAction = (
    data: T | undefined,
    dialogType: DialogVariant | undefined,
  ) => {
    if (data) setSelectedData(data);
    handleShowDialog(dialogType);
  };

  return { selectedDialog, handleAction, selectedData };
};

export { useDialogWithActions };
