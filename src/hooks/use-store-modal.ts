import { create } from "zustand";

interface useStoreModalProps {
  isOpen: boolean;
  onHandleOpen: () => void;
  onHandleClose: () => void;
}

export const useStoreModal = create<useStoreModalProps>((set) => ({
  isOpen: false,
  onHandleOpen: () => set({ isOpen: true }),
  onHandleClose: () => set({ isOpen: false }),
}));
