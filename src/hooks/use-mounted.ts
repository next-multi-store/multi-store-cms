import { useEffect, useState } from "react";

export function useMounted() {
  const [isMounted, setIsMounted] = useState<boolean>();

  useEffect(() => setIsMounted(true), []);

  return { isMounted };
}
