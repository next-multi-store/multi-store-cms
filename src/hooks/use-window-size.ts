/* eslint-disable react-hooks/exhaustive-deps */

'use client';

import { useEffect, useState } from 'react';

interface WindowSize {
  width: number;
  height: number;
  isMobile: boolean;
}

type OptionProps = {
  minWidth?: number;
};

export function useWindowSize(option?: OptionProps): WindowSize {
  const minWidth = option?.minWidth || 1024;

  const [windowSize, setWindowSize] = useState<WindowSize>(() => {
    if (typeof window === 'undefined') {
      return { width: 0, height: 0, isMobile: true };
    }

    const width = window.innerWidth;
    const height = window.innerHeight;
    const isMobile = width < minWidth;

    return { width, height, isMobile };
  });

  function handleResize() {
    const width = window.innerWidth;
    const height = window.innerHeight;
    const isMobile = width < minWidth;

    setWindowSize({ width, height, isMobile });
  }

  useEffect(() => {
    handleResize(); // Run once to get initial value

    window.addEventListener('resize', handleResize);

    // Cleanup function to remove the event listener when the component unmounts
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []); // Empty dependency array ensures that effect runs only on mount and unmount

  return windowSize;
}
